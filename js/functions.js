$( document ).ready(function(){
	
	//Leer Valores
	var base_url = $('#base_url').val();
	var iduser = $('#iduser').val();
	
	$('.button-collapse').sideNav();
	
	$('.slider').slider();
	
	$('.dropdown-button-temporadas').dropdown({
		inDuration: 300,
		outDuration: 225,
		constrainWidth: true, // Does not change width of dropdown to that of the activator
		hover: false, // Activate on hover
		gutter: 0, // Spacing from edge
		belowOrigin: true, // Displays dropdown below the button
		alignment: 'left', // Displays dropdown with edge aligned to the left of button
		stopPropagation: false // Stops event propagation
	});
	
	$( document ).on('click','.dropdown-button',function(e) {
        $(this).dropdown({
			inDuration: 300,
			outDuration: 225,
			constrainWidth: false, // Does not change width of dropdown to that of the activator
			hover: false, // Activate on hover
			gutter: 0, // Spacing from edge
			belowOrigin: true, // Displays dropdown below the button
			alignment: 'left', // Displays dropdown with edge aligned to the left of button
			stopPropagation: false // Stops event propagation
		});
    });
	
	$('.modal').modal();
	$('.modal-mobile').modal();
	
	$(document).on('click','.btn-close-info', function(e) {
		var container = $(this).attr('container');
        $(container).hide();
    });
    
	$('.show').on('click',function(e) {
		
		var ID = $(this).attr('rel');
		var container = $(this).attr('container');
		var loader = $(this).attr('loader');
		var parametros = '{"msg": "getProgramInfo","fields": {"ID": "' + ID + '","container":"' + container + '"}}';
		$(loader).show();
		$(container).html('');
			
		//Request API
		$.post(base_url + '/api', { param: parametros }).done(function( data ) {
			
			//Check Result
			if (data.status == 1)
			{
				//Show HTML Response
				$(container).html(data.html);
				
				//Ocultar Loader
				$(container + ' .dropdown-button').trigger('click');
				
				//Mostramos Contenido
				$(loader).hide();
				$(container).show();
				$(document).scrollTop( $(container).offset().top );  
			}
			else
			{
				//Show Error
				Materialize.toast(data.msg, 4000);
			}
			
		});
    });
    
    $('select').material_select();
    
    //contenedor-btn-favorito click
    $(document).on('click', '.contenedor-btn-favorito', function(e) {
	    e.preventDefault();
	    
	    //Leemos el Programa
	    var idprograma = $(this).attr('rel');
	    
	    //Creamos la llamada
		var param = '{"msg": "addFavorite","fields": {"usuario": "' + iduser + '", "programa": "' + idprograma + '"}}';
		
		//Verificamos si el usuario esta en session
		if (iduser.toString() != '0')
		{
			//API Call
			$.post(base_url + '/api', { param: param }).done(function( data ) {
				
				console.log(data);
				
				//Check Status Call
				if (data.status == 1)
				{
					Materialize.toast('Programa agregado a Favoritos.', 4000);
				}
				else
				{
					//Show Error
					Materialize.toast(data.msg, 4000);
				}
			});
		}
		else
		{
			Materialize.toast('Tienes que iniciar sesión para agregar programas a Favoritos.', 4000);
		}
	    
	    return false;
    });
    
    //Slick featured
    $('.carrusel-edit').slick({
	    infinite: false,
		slidesToShow: 4,
		slidesToScroll: 4,
		dots: false,
		arrows: true,
		responsive: [
		    {
		    	breakpoint: 992,
				settings: {
		        	arrows: false,
					slidesToShow: 3,
					slidesToScroll: 3
		    	}
		    },
		    {
		    	breakpoint: 600,
				settings: {
		        	arrows: false,
					slidesToShow: 2,
					slidesToScroll: 2
		    	}
		    }
		]
    });
    
    //Search Input
    $('#s').keyup(function(e) {
	    e.preventDefault();
	    var text = $.trim($('#s').val());
	    
	    if(e.keyCode == 13) 
	    {
	        if (text)
		    {
		    	window.location.href = base_url + '/?s=' + text;
		    }
		    else
		    {
			    Materialize.toast('Debes de escribir el nombre de un programa para buscar', 4000);
		    }
	    }
	    
	    return false;
	});
    
    //Search Programa
    $('.icon-busqueda').on('click', function(e) {
	    e.preventDefault();
	    var text = $.trim($('#s').val());
	    
	    if (text)
	    {
	    	window.location.href = base_url + '/?s=' + text;
	    }
	    else
	    {
		    Materialize.toast('Debes de escribir el nombre de un programa para buscar', 4000);
	    }
	    
	    return false;
    });
    
    //Close Modal
    $('.btnModalClose').on('click', function(e) {
		e.preventDefault();
		
		$('.modal').modal('close');
		
		return false; 
    });
    
    $('.datepicker').pickadate({
    	selectMonths: true, // Creates a dropdown to control month
		selectYears: 100, // Creates a dropdown of 15 years to control year,
		today: '',
		clear: 'Borrar',
		close: 'Aceptar',
		monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
		weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
		format: 'yyyy/mm/dd',
		formatSubmit: 'yyyy/mm/dd',
		min: new Date(1900,1,1),
		max: true,
		closeOnSelect: false // Close upon selecting a date,
	});
	
	//contenedor-reprodicir-slider Click
	$('.contenedor-reprodicir-slider').on('click', function(e) {
		e.preventDefault();
		
		//Leemos los datos
		var nombre = $.trim($('#inputNombre').val());
		var email = $.trim($('#inputEmail').val());
		var password = $.trim($('#inputPassword').val());
		var confirm = $.trim($('#inputConfirm').val());
		var genero = $('#inputGenero').val();
		var fecha = $('#inputFecha').val();
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		
		var tematicas = new Array();
		$.each($('input[type="checkbox"]:checked'), function (key, value) {
		    tematicas.push($(value).val());
		}); 
		
		var acepto = false;
		if ($('#acepto').is(":checked")) { acepto = true; }
		
		var notificacion = '0';
		if ($('#notificacion').is(":checked")) { notificacion = '1'; }
		
		//Creamos la llamada
		var param = '{"msg": "createUser","fields": {"nombre": "' + nombre + '", "email": "' + email + '", "password": "' + password + '", "genero": "' + genero + '", "fecha": "' + fecha + '", "tematicas": "' + tematicas + '", "notificacion": "' + notificacion + '"}}';
		
		//Verificamos Terminos y Condiciones
		if (acepto)
		{
			//Verificamos nombre de usuario
			if (nombre)
			{
				//Verificamos el Correo Electrónico
				if (reg.test(email)) 
				{
					//Verificamos que exista el Password
					if (password)
					{
						//Verificamos que el Password este confirmado
						if (password == confirm)
						{
							//Verificamos que el Genero este seleccionado
							if (genero)
							{
								//Verificamos la Fecha de Nacimiento
								if (fecha)
								{
									//Disable Button
									$('.contenedor-reprodicir-slider').prop( 'disabled', true );
									$('.contenedor-reprodicir-slider span').text('PROCESANDO...');
		
									//API Call
									$.post(base_url + '/api', { param: param }).done(function( data ) {
										
										//Check Status Call
										if (data.status == 1)
										{
											//Redirect to Gracias
											window.location.href = base_url;
										}
										else
										{
											//Show Error
											Materialize.toast(data.msg, 4000);
					
											//Enable Button
											$('.contenedor-reprodicir-slider').prop( 'disabled', false );
							                $('.contenedor-reprodicir-slider span').html('ACEPTAR');
										}
									});
								}
								else
								{
									Materialize.toast('Selecciona tu fecha de nacimiento.', 4000);
								}
							}
							else
							{
								Materialize.toast('Selecciona tu género.', 4000);
							}
						}
						else
						{
							Materialize.toast('La confirmación de contraseña no coincide con la contraseña.', 4000);
						}
					}
					else
					{
						Materialize.toast('Escribe una contraseña.', 4000);
					}
				}
				else
				{
					Materialize.toast('Escribe un correo electrónico válido.', 4000);
				}
			}
			else
			{
				Materialize.toast('Debes de introducir un nombre de usuario, máximo de 15 caracteres y en minúsculas.', 4000);
			}
		}
		else
		{
			Materialize.toast('Debes aceptar los términos y condiciones.', 4000);
		}
		
		return false;
	});
	
	//registro click
	$('.registro').on('click', function(e) {
		e.preventDefault();
		
		var windowsize = $(window).width();
		
		if (windowsize > 992)
		{
			$('.modal').modal('open');
		}
		else
		{
			window.location.href = base_url + '/registro';
		}
		
		return false;
	});
	
	//doLogin Click
	$('.doLogin').on('click', function(e) {
		e.preventDefault();
		
		//Leemos los datos
		var email = $.trim($('#inputEmail').val());
		var password = $.trim($('#inputPassword').val());
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		
		//Creamos la llamada
		var param = '{"msg": "doLogin","fields": {"email": "' + email + '", "password": "' + password + '"}}';
		
		//Verificamos el Correo Electrónico
		if (reg.test(email)) 
		{
			//Verificamos que exista el Password
			if (password)
			{
				//Disable Button
				$('.doLogin').prop( 'disabled', true );
				$('.doLogin span').text('PROCESANDO...');

				//API Call
				$.post(base_url + '/api', { param: param }).done(function( data ) {
					console.log(data);
					
					//Check Status Call
					if (data.status == 1)
					{
						//Redirect to Gracias
						window.location.href = base_url;
					}
					else
					{
						//Show Error
						Materialize.toast(data.msg, 4000);

						//Enable Button
						$('.doLogin').prop( 'disabled', false );
		                $('.doLogin span').html('ENTRAR');
					}
				});
			}
			else
			{
				Materialize.toast('Escribe una contraseña.', 4000);
			}
		}
		else
		{
			Materialize.toast('Escribe un correo electrónico válido.', 4000);
		}
		
		return false;
	});
	
	//doForgot Click
	$('.doForgot').on('click', function(e) {
		e.preventDefault();
		
		//Leemos los datos
		var email = $.trim($('#inputEmail').val());
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		
		//Creamos la llamada
		var param = '{"msg": "doForgot","fields": {"email": "' + email + '"}}';
		
		//Verificamos el Correo Electrónico
		if (reg.test(email)) 
		{
			//Disable Button
			$('.doForgot').prop( 'disabled', true );
			$('.doForgot span').text('PROCESANDO...');

			//API Call
			$.post(base_url + '/api', { param: param }).done(function( data ) {
				console.log(data);
				
				//Check Status Call
				if (data.status == 1)
				{
					//Show Message
					Materialize.toast('Se han enviado las instrucciones a tu correo electrónico.', 4000);
				}
				else
				{
					//Show Error
					Materialize.toast(data.msg, 4000);
				}
				
				//Enable Button
				$('.doForgot').prop( 'disabled', false );
                $('.doForgot span').html('ENVIAR');
                $('#inputEmail').val('');
			});
		}
		else
		{
			Materialize.toast('Escribe un correo electrónico válido.', 4000);
		}
		
		return false;
	});
	
	//doReset click
	$('.doReset').on('click', function(e) {
		e.preventDefault();
		
		//Leemos los datos
		var nickname = $('#nickname').val();
		var token = $('#token').val();
		var password = $.trim($('#inputPassword').val());
		var confirm = $.trim($('#inputConfirm').val());
		
		//Creamos la llamada
		var param = '{"msg": "doReset","fields": {"password": "' + password + '", "nickname": "' + nickname + '", "token": "' + token + '"}}';
		
		//Verificamos que exista el Password
		if (password)
		{
			//Verificamos que el Password este confirmado
			if (password == confirm)
			{
				//Disable Button
				$('.doReset').prop( 'disabled', true );
				$('.doReset span').text('PROCESANDO...');
	
				//API Call
				$.post(base_url + '/api', { param: param }).done(function( data ) {
					
					//Check Status Call
					if (data.status == 1)
					{
						//Show Message
						Materialize.toast('Se ha cambiado tu contraseña correctamente. Inicia sesión.', 4000);
					}
					else
					{
						//Show Error
						Materialize.toast(data.msg, 4000);
					}
					
					//Enable Button
					$('.doReset').prop( 'disabled', false );
	                $('.doReset span').html('RESTABLECER');
	                $('#inputPassword').val('');
	                $('#inputConfirm').val('');
				});
			}
			else
			{
				Materialize.toast('La confirmación de contraseña no coincide con la nueva contraseña.', 4000);
			}
		}
		else
		{
			Materialize.toast('Escribe una contraseña.', 4000);
		}
		
		return false;
	});
	
	var player = plyr.setup('.js-player');
	
	if ( $('.js-player').length )
	{
		var video_id = $('#video_id').val();
		
		//Reproducción
		player[0].on('playing', function() {
			//console.log('playing - ' + player[0].getCurrentTime());
			//var embed = player[0].getEmbed();
			
			//Verificamos si el Usuario esta logueado
			if (iduser.toString() != '0')
			{
				//Agregamos el Video a la lista
				var parametros = '{"msg": "updateVideoLog","fields": {"video_id": "' + video_id + '","user_id":"' + iduser + '", "action": "play"}}';
				
				//Request API
				$.post(base_url + '/api', { param: parametros }).done(function( data ) { console.log(data); } );
			}
		});
		
		//Video Terminado
		player[0].on('ended', function() {
			//console.log('ended - ' + player[0].getCurrentTime());
			
			//Verificamos si el Usuario esta logueado
			if (iduser.toString() != '0')
			{
				//Agregamos el Video a la lista
				var parametros = '{"msg": "updateVideoLog","fields": {"video_id": "' + video_id + '","user_id":"' + iduser + '", "action": "ended"}}';
				
				//Request API
				$.post(base_url + '/api', { param: parametros }).done(function( data ) { console.log(data); } );
			}
		});
		
		//En Pausa
		player[0].on('pause', function() {
			//console.log('pause - ' + player[0].getCurrentTime());
			
			//Verificamos si el Usuario esta logueado
			if (iduser.toString() != '0')
			{
				//Agregamos el Video a la lista
				var parametros = '{"msg": "updateVideoLog","fields": {"video_id": "' + video_id + '","user_id":"' + iduser + '", "action": "pause"}}';
				
				//Request API
				$.post(base_url + '/api', { param: parametros }).done(function( data ) { console.log(data); } );
			}
		});
		
		//En Busqueda
		player[0].on('seeked', function() {
			//console.log('seeked - ' + player[0].getCurrentTime());
			
			//Verificamos si el Usuario esta logueado
			if (iduser.toString() != '0')
			{
				//Agregamos el Video a la lista
				var parametros = '{"msg": "updateVideoLog","fields": {"video_id": "' + video_id + '","user_id":"' + iduser + '", "action": "seeked"}}';
				
				//Request API
				$.post(base_url + '/api', { param: parametros }).done(function( data ) { console.log(data); } );
			}
		});
		
		//Abrir Pantalla Completa
		player[0].on('enterfullscreen', function() {
			//console.log('enterfullscreen - ' + player[0].getCurrentTime());
			
			//Verificamos si el Usuario esta logueado
			if (iduser.toString() != '0')
			{
				//Agregamos el Video a la lista
				var parametros = '{"msg": "updateVideoLog","fields": {"video_id": "' + video_id + '","user_id":"' + iduser + '", "action": "enterfullscreen"}}';
				
				//Request API
				$.post(base_url + '/api', { param: parametros }).done(function( data ) { console.log(data); } );
			}
		});
		
		//Cerrar Pantalla Completa
		player[0].on('exitfullscreen', function() {
			//console.log('exitfullscreen - ' + player[0].getCurrentTime());
			
			//Verificamos si el Usuario esta logueado
			if (iduser.toString() != '0')
			{
				//Agregamos el Video a la lista
				var parametros = '{"msg": "updateVideoLog","fields": {"video_id": "' + video_id + '","user_id":"' + iduser + '", "action": "exitfullscreen"}}';
				
				//Request API
				$.post(base_url + '/api', { param: parametros }).done(function( data ) { console.log(data); } );
			}
		});
	}
	
	//Player btnClose click
	$(".btnClose").click(function(event) {
		event.preventDefault();
		var links = history.length;
		var video_id = $('#video_id').val();
		
		//Verificamos si el Usuario esta logueado
		if (iduser.toString() != '0')
		{
			//Agregamos el Video a la lista
			var parametros = '{"msg": "updateVideoLog","fields": {"video_id": "' + video_id + '","user_id":"' + iduser + '", "action": "close"}}';
			
			//Request API
			$.post(base_url + '/api', { param: parametros }).done(function( data ) { console.log(data); } );
		}
		
		if (links.toString() > '2')
		{
			history.back(1);
		}
		else
		{
			window.location.href = base_url;
		}
	});
	
	//Slick Videos
    $('.my-videos').slick({
	    infinite: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
		responsive: [
		    {
		    	breakpoint: 992,
				settings: {
		        	arrows: false,
					slidesToShow: 3,
					slidesToScroll: 3
		    	}
		    },
		    {
		    	breakpoint: 600,
				settings: {
		        	arrows: false,
					slidesToShow: 2,
					slidesToScroll: 2
		    	}
		    }
		]
    });
  
})
