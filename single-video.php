<?php global $post; ?>

<?php get_header(); ?>

		<?php $programa = get_field("programa", $post->ID); ?>
		<div class="container-fluid">
			<div class="row no-margin-row">
				<div class="col s12 no-padding">
					<div class="video-container">
						<div id="video-wrapper">
							<input type="hidden" id="video_id" name="video_id" value="<?php echo $post->ID; ?>">
							<div class="js-player" data-type="youtube" data-video-id="<?php the_field("youtube_id", $post->ID); ?>"></div>
							<div class="closeVideo">
								<a class="btn-flat btnClose">
									<i class="fa fa-times-circle-o font30 magenta-text" aria-hidden="true"></i>
								</a>
							</div>
						</div>
      				</div>
				</div>
			</div>
		</div>
		
		<?php get_template_part("includes/search","menuv2"); ?>
		
		<?php
			//Procesamos las Variables del programa
			$itunes = get_field("itunes", $programa->ID);
			$plataforma = get_field("plataforma", $programa->ID);
			$contenido = get_field("contenido",$programa->ID );
			$logo = get_field("logo",$programa->ID);
			$portada = get_field("portada",$programa->ID);
			$info = get_field("info",$programa->ID);
			$excerpt = $programa->post_excerpt;
			if (!$excerpt) { $excerpt = $programa->post_content; }
			$contenido_programa_string = (isset($_GET['content'])) ? (string)trim($_GET['content']) : '';
			$contador_contenido = 0;
			$siguiente = '';	
		?>
		<div class="container-fluid episodios-series">
			<div class="row no-margin-row">
				<div class="col s12 m12 l4 hide-on-med-and-down">
					<div class="cont-img-serie">
						<img class="responsive-img" alt="<?php echo $programa->post_title; ?>" longdesc="<?php echo $logo; ?>" src="<?php echo $logo; ?>">
						<div class="space40"></div>
					</div>
				</div>
				<div class="col s12 m12 l8">
					<div class="space20 hide-on-med-and-down"></div>
						<div class="col s12 m12 l12">
							<div class="col s12 hide-on-large-only" style="padding: 0 2px;">
								<p class="roboto font18 white-text mobil-text-justify"><?php echo $excerpt; ?></p>
							</div>
						</div>
						<div class="col s12 m6 l6">
							<div class="col s6 m6 l5 hide-on-large-only" style="padding: 0 2px;">
								<a href="<?php echo $siguiente; ?>">
									<div class="btnGreen centered">
										<i class="fa fa-play-circle-o font24 white-text" aria-hidden="true"></i>
										<span class="roboto font22 gray-text">Reproducir</span>
									</div>
								</a>
							</div>
							<div class="col s6 m6 l8" style="padding: 0 2px;">
								<div class="btnGreen centered">
									<a class="dropdown-button btn-temporadas-slider btn font22 roboto no-padding gray-text" href="#" data-activates="contenido_<?php echo $post->ID; ?>" style="margin-top: 8px;">Temporadas<i class="fa fa-caret-down" aria-hidden="true" style="padding-left: 5px;"></i></a>
									<ul id="contenido_<?php echo $post->ID; ?>" class="dropdown-content black">
										<?php foreach ($contenido as $item) { $contador_contenido++; if ($contador_contenido==1) { if (!$contenido_programa_string) { $contenido_programa_string = sanitize_title($item['nombre']); } } ?>
										<li><a class="white-text" href="<?php echo get_permalink($post->ID); ?>?content=<?php echo sanitize_title($item['nombre']); ?>"><?php echo $item['nombre']; ?></a></li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="col s12 m6 l6">
							<div class="space10 hide-on-med-and-up"></div>
							<div class="col s6 m6 l5 hide-on-large-only">
								<a href="#">
									<div class="contenedor-btn-favorito centered" rel="<?php echo $post->ID; ?>">
										<i class="fa fa-plus-circle white-text btn-fav-img-serie" title="Boton Play" aria-hidden="true"></i>
										<span class="roboto font22 white-text uppercase">FAVORITOS</span>
									</div>
								</a>
							</div>
							<div class="col s6 m6 l8 float-right-serie" style="padding: 0 2px;">
								<a href="<?php bloginfo("url"); ?>">
									<div class="btnGreen centered">
										<span class="roboto font22 gray-text">Volver al Inicio</span>
									</div>
								</a>
							</div>
						</div>
						<br />
						<div class="col s12 m6 l6">
							<div class="col s6 m6 l5" style="padding: 0 2px;">
								<div class="contenedor-btn-social centered" rel="<?php echo $post->ID; ?>">
									<a href="#" class="fb_share" rel="<?php echo get_permalink($post->ID); ?>?utm_source=facebook" title="<?php echo $post->post_title; ?>">
										<span class="roboto font14 white-text uppercase">COMPARTIR EN</span>&nbsp;
										<i class="fa fa-facebook-official fa-lg white-text" aria-hidden="true"></i>
									</a>
								</div>
							</div>
							<div class="col s6 m6 l5" style="padding: 0 2px;">
								<div class="contenedor-btn-social centered" rel="<?php echo $post->ID; ?>">
									<a href="#" class="tw_share" rel="<?php echo get_permalink($post->ID); ?>?utm_source=twitter" title="<?php echo $post->post_title; ?>">
										<span class="roboto font14 white-text uppercase">COMPARTIR EN</span>&nbsp;
										<i class="fa fa-twitter fa-lg white-text" aria-hidden="true"></i>
									</a>
								</div>
							</div>
						</div>
						<?php if ($contenido) { ?>
							<?php
							
								$contenido_programa = array();
								$contador_contenido_programa = 0;
								foreach ($contenido as $item)
								{
									$contador_contenido_programa++;
									if (sanitize_title($item['nombre']) == $contenido_programa_string)
									{
										$contenido_programa = $item;
										break;
									}
									
									if (count($contenido) == $contador_contenido_programa)
									{
										$contenido_programa = $item;
										break;
									}
								}
							?>
							<div class="col s12 m12 l12">
								<div class="space20"></div>
								<span class="roboto bold white-text font35 uppercase"><?php echo $contenido_programa['nombre']; ?></span>
								<div class="right">
									<label id="lblSelectOrdenPrograma" for="selectOrdenPrograma"></label>
									<select class="browser-default btnGreen" name="selectOrdenPrograma" id="selectOrdenPrograma" rel="<?php echo $programa->ID; ?>" contenido="<?php echo $contenido_programa_string; ?>">
									    <option value="ASC"selected>Fecha de tx</option>
									    <option value="DESC">Más recientes</option>
									</select>
								</div>
								<div class="space10"></div>
							</div>
							<div class="contenido_programa">
								<div class="col s12 m10 offset-m1 l12" style="position: relative; padding-bottom: 45px;">
									<a href="#" aria-label="link" title="Boton Inicio de Página" role="button">
										<div class="btn-arriba">
											<i class="fa fa-chevron-up white-text" aria-hidden="true" style="font-size: 3rem;"></i>
											<span style="right: 101%; font-size: 0; width: 1em; height: 1em; display: inline-block; overflow: hidden; border: 0!important; padding: 0!important; margin: 0!important;">Boton Arriba</span>
										</div>
									</a>
									<?php foreach ($contenido_programa['videos'] as $video) { ?>
									<?php
										$excerpt_video = $video->post_excerpt;
										if (!$excerpt_video) { $excerpt_video = $video->post_content; }
									?>
									<!--<a href="<?php echo get_permalink($video->ID); ?>">-->
										<div class="space40"></div>
										<div class="row">
											<div class="col s12 m4 l3">
												<a href="<?php echo get_permalink($video->ID); ?>" class="contenedor-reproducir-episodio-serie" style="display: block; margin-top: 1em;">
													<img class="btn-play-serie" alt="Reproducir Video" longdesc="<?php bloginfo("template_directory"); ?>/img/img_episodios_cenado_btn_play.png" src="<?php bloginfo("template_directory"); ?>/img/img_episodios_cenado_btn_play.png">
													<img class="responsive-img-full-w-h" alt="<?php echo $video->post_title; ?>" longdesc="https://img.youtube.com/vi/<?php the_field("youtube_id", $video->ID); ?>/mqdefault.jpg" src="https://img.youtube.com/vi/<?php the_field("youtube_id", $video->ID); ?>/mqdefault.jpg">
												</a>
											</div>
											<div class="col s12 m8 l9">
												<p style="text-align: justify;">
												<span class="titulo-episodio roboto bold font23 white-text block"><?php echo $video->post_title; ?></span>
												<br>
												<span class="descripcion-episodio roboto font16 white-text"><?php echo $excerpt_video; ?></span>
												</p>
												<div class="space40"></div>
											</div>
										</div>
									<!--</a>-->
									<?php } ?>
								</div>
							</div>
						<?php } ?>
				</div>
			</div>
		</div>

		<?php
			//CONSULTAMOS ALEATORIAMENTE 8 ELEMENTOS
			$args = array(
				'post_type' => 'programa',
				'post__not_in' => array($programa->ID),
				'order' => 'ASC',
				'orderby' => 'rand',
				'posts_per_page' => 8
			); 
			$randoms = new WP_Query( $args ); 
		?>
		<div class="container-fluid">
			<div class="row no-margin-row">
				<div class="contenedor-titulo-carrusel-interes">
					<span class="opns-bold-italic font22 green-text uppercase">
						también
					</span>
					<br>
					<span class="opns-bold-italic font38 white-text uppercase">
						te puede interesar
					</span>
					<div class="space20"></div>
				</div>
			</div>
			<div class="row carrusel-edit no-margin-row">
				<?php foreach ($randoms->posts as $item) { ?>
				<div class="col s6 m6 l3 no-padding">
					<div class="contenedor-img-carrusel">
						<!--<a href="<?php echo get_permalink($item->ID); ?>" container=".info-serie-footer" loader=".loader-footer">-->
						<a href="<?php echo get_permalink($item->ID); ?>">
							<div class="hover-ver-mas">
								<i class="fa fa-plus-circle white-text" aria-hidden="true"></i>
								<p class="avenir-next-regular font20 white-text">VER MÁS</p>
							</div>
							<img class="responsive-img" alt="<?php echo $item->post_title; ?>" longdesc="<?php the_field("logo",$item->ID); ?>" src="<?php the_field("logo",$item->ID); ?>">
						</a>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="row loader-footer centered" style="display: none;">
				<div class="space40"></div>
				<div class="preloader-wrapper big active">
					<div class="spinner-layer spinner-white-only">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
				</div>
				<div class="space40"></div>
			</div>
			<div class="row info-serie-footer"></div>
		</div>

<?php get_footer(); ?>