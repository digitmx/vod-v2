
		<!--Programacion y Buscador-->
		<?php
			/* Leemos los Grupos */
			$grupos = get_terms( array(
			    'taxonomy' => 'grupo',
			    'hide_empty' => false,
			) );
			
			/* Leemos las Tematicas */
			$tematicas = get_terms( array(
			    'taxonomy' => 'tematica',
			    'hide_empty' => false,
			) );	
		?>
		<div class="container-fluid black">
			<div class="row">
				<div class="col l5 hide-on-med-and-down">
					<div class="col s12 m12 l7 no-padding centered" style="line-height: 70px">
						<!-- Dropdown Trigger -->
						<a class="dropdown-button btn font22 roboto black no-padding" href="#" data-activates="grupos" style="margin-top: 20px;">PROGRAMAS A - Z<!--<i class="small material-icons">arrow_drop_down</i>--><i class="fa fa-caret-down" aria-hidden="true" style="padding-left: 5px;"></i></a>
						
						<!-- Dropdown Structure -->
						<ul id="grupos" class="dropdown-content black">
							<?php foreach ($grupos as $term) { ?>
							<li><a class="white-text" href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
							<?php } ?>
						</ul>
					</div>
					<div class="col s12 m12 l5 no-padding centered" style="line-height: 70px;">
						<!-- Dropdown Trigger -->
						<a class="dropdown-button btn font22 roboto black uppercase no-padding" href="#" data-activates="tematicas" style="margin-top: 20px;">categorías<i class="fa fa-caret-down" aria-hidden="true" style="padding-left: 5px;"></i></a>
						
						<!-- Dropdown Structure -->
						<ul id="tematicas" class="dropdown-content black">
							<?php foreach ($tematicas as $term) { ?>
							<li><a class="white-text" href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<form id="formSearch" name="formSearch" method="get" action="<?php bloginfo("url"); ?>">
					<div class="col s10 m11 l6">
						<div class="input-field">
							<input id="s" name="s" class="editar-busqueda" type="search" value="<?=(isset($_GET['s'])) ? (string)trim($_GET['s']) : ''; ?>" placeholder="Introduce el nombre del programa que deseas buscar">
							<label class="label-icon" for="s"><i class="material-icons"></i></label>
						</div>
					</div>
					<div class="col s2 m1 l1">
						<button type="submit" style="background-color: transparent; border: 0px;">
							<i class="fa fa-search white-text icon-busqueda" aria-hidden="true"></i>
						</button>
					</div>
				</form>
			</div>
		</div>