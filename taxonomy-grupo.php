<?php
	global $term;	
	$categoria = get_term_by('slug', $term, 'grupo');
	
	//Consulta de Posts de la Categoría
	$args = array(
		'post_type' => 'programa',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'orderby' => 'title',
		'tax_query' => array(
			array(
				'taxonomy' => 'grupo',
				'field'    => 'term_id',
				'terms'    => $categoria->term_id,
			),
		),
	); 
	$query = new WP_Query( $args ); 
?>

<?php get_header(); ?>

		<?php $banners = get_field("banners", "option"); $contador_banners = 0; $programs_show = array(); ?>
		<?php include(locate_template('includes/slider-home.php')); ?>
		
		<?php get_template_part("includes/search","menuv2"); ?>
		
		<!--Series Categoria-->
		<div class="container-fluid categoria">
			<div class="row no-margin-row">
				<div class="col s12 m12 l12 padding-favoritos-series">
					<div class="space40"></div>
					<div class="contenedor-titulo-categorias">
						<span class="opns-bold-italic font36 white-text uppercase"><?php echo $categoria->name; ?></span>
						<div class="space20"></div>
					</div>
					<div class="space40"></div>
					<div class="col s12 m12 l12 contenedor-favoritos-series">
						<div class="col s12 m12 l12" style="padding: 0;">
							<?php foreach( $query->posts as $programa ) { $portada = get_field( 'logo', $programa->ID );  ?>
							<div class="col s6 m4 l2" style="padding-left: 0;">
								<div class="contenedor-img-favoritos">
									<a title="Programa" href="<?php echo get_permalink($programa->ID); ?>" tabindex="0">
										<span style="right: 101%; font-size: 0; width: 1em; height: 1em; display: inline-block; overflow: hidden; border: 0!important; padding: 0!important; margin: 0!important;">Portada Programa</span>
										<img class="responsive-img" alt="Portada" longdesc="<?php echo $portada; ?>" src="<?php echo $portada; ?>">
									</a>
								</div>
								<div class="space20"></div>
							</div>
							<?php } ?>
						</div>
						<div class="col s12 m12 l12 no-padding">
						<div class="space40"></div>
						<div class="col s12 m12 l12 no-padding">
							<div class="space40"></div>
							<div class="col s6 m4 l4 offset-s3 offset-m4 offset-l4">
								<a href="<?php bloginfo("url"); ?>">
									<div class="btnGreen centered">
										<span class="roboto font22 gray-text">Volver al Inicio</span>
									</div>
								</a>
								<div class="space40"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>