<?php /* Template Name: Recuperar Contraseña */ ?>

<?php $wp_session= WP_Session::get_instance(); ?>

<?php if (!$wp_session['logged']) { ?> 

<?php get_header(); ?>

<div class="container-fluid registro-usuario">
	<div class="row no-margin-row">
		<div class="col s12 m12 l6 offset-l3">
			<form id="formRegistro" name="formRegistro" accept-charset="utf-8" method="post" action="<?php bloginfo("url"); ?>">
				<div class="space40"></div>
				<div class="col s12 m12 l12">
					<div class="contenedor-titulo-registro">
						<span class="opns-bold-italic font22 green-text uppercase" style="margin-left: 8px;">
							Recuperar
						</span>
						<br>
						<span class="opns-bold-italic font38 white-text uppercase">
							Contraseña
						</span>
						<div class="space40"></div>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m8 offset-m2 l6 offset-l3 no-padding">
						<div class="input-field col s12">
							<input autocomplete="off" placeholder="Escribe tu correo electrónico registrado y sigue las instrucciones" id="inputEmail" name="inputEmail" type="email" class="validate inp-registro">
							<label class="opns-bold-italic font20 white-text uppercase inp-etiqueta" for="inputEmail">correo electrónico</label>
						</div>
					</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 opns-regular uppercase editar-checkbox">
						<div class="col s12 m12 l12 no-padding">
							<div class="space40"></div>
							<div class="col s5 m3 l3" style="float: none; margin: 0 auto;">
								<div class="btnGreen centered doForgot">
									<span class="roboto font22 gray-text uppercase">enviar</span>
								</div>
								<div class="space40"></div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<?php get_footer(); ?>

<?php } else { wp_redirect( home_url() ); exit; } ?>