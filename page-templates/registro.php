<?php /* Template Name: Registro Usuarios */ ?>

<?php $wp_session= WP_Session::get_instance(); ?>

<?php if (!$wp_session['logged']) { ?> 

<?php 
	global $post; 
	
	/* Leemos las Tematicas */
	$tematicas = get_terms( array(
	    'taxonomy' => 'tematica',
	    'hide_empty' => false,
	) );
?>


<?php get_header(); ?>

<?php if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>

<div class="container-fluid registro-usuario">
	<div class="row no-margin-row">
		<div class="col s12 m12 l10 offset-l1">
			<form id="formRegistro" name="formRegistro" accept-charset="utf-8" method="post" action="<?php bloginfo("url"); ?>">
				<div class="space40"></div>
				<div class="col s12 m12 l12">
					<div class="contenedor-titulo-registro">
						<span class="opns-bold-italic font22 green-text uppercase" style="margin-left: 8px;">
							registro
						</span>
						<br>
						<span class="opns-bold-italic font38 white-text uppercase">
							de usuario
						</span>
						<div class="space40"></div>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m6 l6 no-padding">
						<div class="input-field col s12">
							<input autocomplete="off" placeholder="" id="inputNombre" name="inputNombre" type="text" maxlength="15" class="validate inp-registro lowercase">
							<label class="opns-bold-italic font20 white-text uppercase inp-etiqueta" for="inputNombre">Nombre de usuario</label>
						</div>
					</div>
					<div class="col s12 m6 l6 no-padding">
						<div class="input-field col s12">
							<input autocomplete="off" placeholder="" id="inputEmail" name="inputEmail" type="email" class="validate inp-registro">
							<label class="opns-bold-italic font20 white-text uppercase inp-etiqueta" for="inputEmail">correo electrónico</label>
						</div>
					</div>
					<div class="col s12 m6 l6 no-padding">
						<div class="input-field col s12">
							<input autocomplete="off" placeholder="" id="inputPassword" name="inputPassword" type="password" class="validate inp-registro">
							<label class="opns-bold-italic font20 white-text uppercase inp-etiqueta" for="inputPassword">contraseña</label>
						</div>
					</div>
					<div class="col s12 m6 l6 no-padding">
						<div class="input-field col s12">
							<input autocomplete="off" placeholder="" id="inputConfirm" name="inputConfirm" type="password" class="validate inp-registro">
							<label class="opns-bold-italic font20 white-text uppercase inp-etiqueta" for="inputConfirm">confirmar contraseña</label>
						</div>
					</div>
					<div class="col s12 m6 l6 no-padding">
						<div class="input-field col s12 block btn-select">
							<select id="inputGenero" name="inputGenero">
								<option value="" disabled selected></option>
								<option value="Hombre">Hombre</option>
								<option value="Mujer">Mujer</option>
							</select>
							<label class="opns-bold-italic font20 white-text uppercase inp-etiqueta">género</label>
						</div>
					</div>
					<div class="col s12 m6 l6 no-padding">
						<div class="input-field col s12 btn-select">
							 <input autocomplete="off" placeholder="" type="text" id="inputFecha" name="inputFecha" class="datepicker inp-registro">
							 <label class="opns-bold-italic font20 white-text uppercase inp-etiqueta inp-fecha" for="inputFecha">fecha de nacimiento</label>
						</div>
					</div>
				</div>
				<div class="col s12 m12 l12">
					<span class="opns-bold-italic font22 white-text uppercase">
						temas preferidos
					</span>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 opns-regular uppercase editar-checkbox">
						<?php foreach ($tematicas as $term) { ?>
						<div class="col s6 m6 l4 no-padding">
							<form action="#">
								<p class="inline">
									<input class="checkTematicas" type="checkbox" id="<?php echo sanitize_title($term->name); ?>" name="<?php echo sanitize_title($term->name); ?>" value="<?php echo $term->term_id; ?>" />
									<label class="white-text text-label-registro" for="<?php echo sanitize_title($term->name); ?>"><?php echo $term->name; ?></label>
								</p>
							</form>
						</div>
						<?php } ?>
					</div>
					<div class="col s12 m12 l12">
						<div class="space40"></div>
						<p class="opns-regular font20 centered white-text uppercase">
							términos y condiciones
						</p>
					</div>
					<div class="col s12 m12 l12">
						<div class="space20"></div>
						<div class="contenedor-texto-terminos">
							<p class="scbar font10 uppercase"><?php the_content(); //echo $post->post_content; ?></p>
						</div>
						<div class="space20"></div>
					</div>
					<div class="col s12 m12 l12 opns-regular uppercase editar-checkbox">
						<div class="col s12 m12 l12 no-padding">
							<form action="#" class="centered">
								<p class="inline">
									<input type="checkbox" id="acepto" name="acepto" value="true" />
									<label class="white-text " for="acepto">acepto términos y condiciones</label>
								</p>
							</form>
						</div>
						<div class="col s12 m12 l12 no-padding">
							<form action="#" class="centered">
								<p class="inline">
									<input type="checkbox" id="notificacion" name="notificacion" value="true" />
									<label class="white-text " for="notificacion">quiero recibir notificaciones</label>
								</p>
							</form>
						</div>
						<div class="col s12 m12 l12 no-padding">
							<div class="space40"></div>
							<div class="col s5 m3 l3" style="float: none; margin: 0 auto;">
								<div class="contenedor-reprodicir-slider centered">
									<span class="roboto font22 gray-text uppercase">aceptar</span>
								</div>
								<div class="space40"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 no-padding centered">
						<span class="opns-bold-italic font22 white-text uppercase">
							Si ya cuentas con un usuario
						</span>
					</div>
					<div class="col s12 m12 l12 no-padding">
						<div class="space40"></div>
						<div class="col s6 m4 l3" style="float: none; margin: 0 auto;">
							<a href="<?php bloginfo("url"); ?>/login">
								<div class="btnGreen centered">
									<span class="roboto font22 gray-text">INICIA SESIÓN</span>
								</div>
							</a>
							<div class="space40"></div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<?php } } ?>

<?php get_footer(); ?>

<?php } else { wp_redirect( home_url() ); exit; } ?>