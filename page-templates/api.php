<?php /* Template Name: API */

	//Include PHPMailer
	include_once( getcwd().'/wp-includes/class-phpmailer.php' );
	
	//Read Param Field
	$json = (isset($_POST['param'])) ? $_POST['param'] : NULL; $output = FALSE;
	$json = str_replace('\\', '', $json);

	//Check JSON
	if ($json != NULL)
	{
		//Decode Data JSON
		$json_decode = json_decode($json, true);

		//Read Action JSON
		$msg = (isset($json_decode['msg'])) ? (string)trim($json_decode['msg']) : '';

		//Read Fields JSON
		$fields = (isset($json_decode['fields'])) ? $json_decode['fields'] : array();

		//getHomeSlider
		if ($msg == 'getHomeSlider')
		{
			//Consultamos los últimos 5 videos publicados
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => 5,
				'order' => 'DESC',
				'orderby' => 'date'	
			); 
			$query_slider = new WP_Query( $args );
			$videos = array();
			
			//Procesamos los Videos
			foreach( $query_slider->posts as $video)
			{
				//Procesamos las Variables
				$youtube_id = get_field( 'youtube_id', $video->ID );
				$lanzamiento = get_field("lanzamiento", $video->ID);
				$array_generos = get_the_terms($video->ID, 'generos'); $generos = '';
				foreach ($array_generos as $genero)
				{
					if ($generos) { $generos = $generos . ',' . $genero->name; }
					else { $generos = $genero->name; }
				}
				
				//Generamos el Arreglo
				$videos[] = array(
					'ID' => $video->ID,
					'title' => $video->post_title,
					'content' => $video->post_content,
					'genres' => $generos,
					'release' => $lanzamiento,
					'youtube_id' => $youtube_id
				);
			}
			
			//Show Results
			$array = array(
				'status' => (int)1,
				'msg' => (string)'success',
				'data' => $videos
			);
	
			//Print JSON Array
			printJSON($array);
			$output = TRUE;
		}
		
		//getVideoModal
		if ($msg == 'getVideoModal')
		{
			//Leemos los Valores
			$ID = (isset($fields['ID'])) ? (string)trim($fields['ID']) : '';
			
			//Verificamos Valores
			if ($ID)
			{
				//Leer el Video
				$video = get_post($ID);
				
				//Procesamos las Variables
				$youtube_id = get_field( 'youtube_id', $video->ID );
				$lanzamiento = get_field("lanzamiento", $video->ID);
				$array_generos = get_the_terms($video->ID, 'generos'); $generos = '';
				foreach ($array_generos as $genero)
				{
					if ($generos) { $generos = $generos . ',' . $genero->name; }
					else { $generos = $genero->name; }
				}
				$year = substr($lanzamiento, 0, 4);
				$month = substr($lanzamiento, 4, 2);
				$day = substr($lanzamiento, 6, 2);
				
				//Generamos el Arreglo
				$result = array(
					'ID' => $video->ID,
					'title' => $video->post_title,
					'content' => $video->post_content,
					'genres' => $generos,
					'release' => $day . '/' . $month . '/' . $year,
					'youtube_id' => $youtube_id,
				);
				
				//Generamos el HTML
				$html = '';
				$html.= '<div class="container-fluid">';
				$html.= '	<div class="contenedor-imagen">';
				$html.= '		<div class="imagen-gradado-programa"></div>';
				$html.= '		<img class="modal-imagen-programa responsive-img" src="'.get_bloginfo('template_directory').'/img/banner01.jpg">';
				$html.= '		<h2 class="titulo-modal-h">'.$video->post_title.'</h2>';
				$html.= '	</div>';
				$html.= '	<div class="modal-text1">';
				$html.= '		<div class="row">';
				$html.= '			<div class="col s12 m8 l8">';
				$html.= '				<p class="d-modal-h">'.$video->post_content.'</p>';
				$html.= '			</div>';
				$html.= '			<div class="col s12 m4 l4">';
				$html.= '				<p class="elenco-programa">Elenco:</p>';
				$html.= '				<p class="genero-programa">Generos:</p>';
				$html.= '				<a class="waves-effect waves-light btn-large dull white-text opensans bold" id="backModal">';
				$html.= '					<i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Ir a VOD';
				$html.= '				</a>';
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '		<div class="row">';
				$html.= '			<div class="col s12 m12 l12">';
				$html.= '				<div class="boton-social-media">';
				$html.= '					<button class="social-m-b" type="button"><i class="fa fa-plus fa-2x" aria-hidden="true"></i></button><br /><span>Mi Lista</span>';
				$html.= '				</div>';
				$html.= '				<div class="boton-social-media">';
				$html.= '					<button class="social-m-b" type="button"><i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true"></i></button><br /><span>Calificar</span>';
				$html.= '				</div>';
				$html.= '				<div class="boton-social-media">';
				$html.= '					<button class="social-m-b" type="button"><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i></button><br /><span>Facebook</span>';
				$html.= '				</div>';
				$html.= '				<div class="boton-social-media">';
				$html.= '					<button class="social-m-b" type="button"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></button><br /><span>Twitter</span>';
				$html.= '				</div>';
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '		<div class="row">';
				$html.= '			<div class="col s12">';
				$html.= '				<!-- Dropdown Structure -->';
				$html.= '				<ul id="temporadas-modal" class="dropdown-content temp-menu-modal">';
				$html.= '					<li><a class="edit-texto" href="#!">Temporada 1</a></li>';
				$html.= '					<li><a class="edit-texto" href="#!">Temporada 2</a></li>';
				$html.= '					<li><a class="edit-texto" href="#!">Temporada 3</a></li>';
				$html.= '				</ul>';
				$html.= '				<a class="dropdown-button-modal btn-modal" href="#" data-activates="temporadas-modal">Temporadas<i class="material-icons posicion-flecha-modal">keyboard_arrow_down</i></a>';
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '	</div>';
				$html.= '</div>';
				$html.= '<div class="container-fluid background-color-episodios">';
				$html.= '	<div class="divider"></div>';
				$html.= '	<div class="row">';
				$html.= '		<br />';
				$html.= '		<div class="col s2 offset-s1 m2 offset-m1 l2 offset-l1">';
				$html.= '			<img class="imagen-episodio responsive-img" src="https://img.youtube.com/vi/'.$youtube_id.'/mqdefault.jpg">';
				$html.= '		</div>';
				$html.= '		<div class="col s8 m8 l8">';
				$html.= '			<div class="titulo-episodio">';
				$html.= '				<h4></h4>';
				$html.= '			</div>';
				$html.= '			<div class="descripcion-episodio">';
				$html.= '				<p></p>';
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '	</div>';
				$html.= '	<div class="divider"></div>';
				$html.= '</div>';
				
				//Show Results
				$array = array(
					'status' => (int)1,
					'msg' => (string)'success',
					'data' => $result,
					'html' => $html
				);
				
				//Print JSON Array
				printJSON($array);
				$output = TRUE;	
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No se pudo procesar la información en este momento.'
				);
		
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//getVideoModal
		if ($msg == 'getProgramaModal')
		{
			//Leemos los Valores
			$ID = (isset($fields['ID'])) ? (string)trim($fields['ID']) : '';
			
			//Verificamos Valores
			if ($ID)
			{
				//Leer el Video
				$programa = get_post($ID);
				
				//Procesamos las Variables
				$itunes = get_field("itunes", $programa->ID);
				$plataforma = get_field("plataforma", $programa->ID);
				$contenido = get_field("contenido",$programa->ID );
				$logo = get_field("logo",$programa->ID);
				$portada = get_field("portada",$programa->ID);
				
				//Generos
				$array_generos = get_the_terms($programa->ID, 'genero'); $generos = '';
				if (is_array($array_generos))
				{
					foreach ($array_generos as $genero)
					{
						if ($generos) { $generos = $generos . ', ' . $genero->name; }
						else { $generos = $genero->name; }
					}
				}	
				
				//Elenco	
				$array_elenco = get_the_terms($programa->ID, 'elenco'); $elenco = '';
				if (is_array($array_elenco))
				{
					foreach ($array_elenco as $row)
					{
						if ($elenco) { $elenco = $elenco . ', ' . $row->name; }
						else { $elenco = $row->name; }
					}
				}
				
				//Generamos el Arreglo
				$result = array(
					'ID' => $programa->ID,
					'title' => $programa->post_title,
					'content' => $programa->post_content,
					'genres' => $generos,
					'staff' => $elenco,
					'videos' => $contenido
				);
				
				//Obtenemos la URL del Video Siguiente
				$siguiente = '';
				if ($contenido) 
				{
					foreach ($contenido as $item)
					{
						foreach ($item['videos'] as $video)
						{
							$siguiente = get_permalink($video->ID);
							break;
						}
					}
				}
				
				//Generamos el HTML
				$html = '';
				$html.= '<div class="container-fluid">';
				$html.= '	<div class="contenedor-imagen">';
				
				if ($siguiente)
				{
					$html.= '		<div class="boton-play-imagen-modal">';
					$html.= '			<a class="boton-play-p" href="' . $siguiente . '" rel="" tabindex="0">';
					$html.= '				<div class="boton-play-pri">';
					$html.= '					<div class="boton-play-pri-anillo"></div>';
					$html.= '					<span class="boton-play-pri-triangulo">';
					$html.= '						<i class="fa fa-play fa-4x" aria-hidden="true"></i>';
					$html.= '					</span>';
					$html.= '				</div>';
					$html.= '			</a>';
					$html.= '		</div>';
				}
					
				$html.= '		<div class="imagen-gradado-programa"></div>';
				$html.= '		<img class="modal-imagen-programa responsive-img" src="'.$portada.'">';
				$html.= '		<h2 class="titulo-modal-h opensans bold">'.$programa->post_title.'</h2>';
				$html.= '	</div>';
				$html.= '	<div class="modal-text1">';
				$html.= '		<div class="row">';
				$html.= '			<div class="col s12 m8 l8">';
				$html.= '				<p class="d-modal-h opensans">'.$programa->post_content.'</p>';
				$html.= '			</div>';
				$html.= '			<div class="col s12 m4 l4">';
				
				if ($elenco) {
					$html.= '				<p class="elenco-programa opensans"><b>Elenco:</b> '.$elenco.'</p>';
				}
				
				if ($generos) {
					$html.= '				<p class="genero-programa opensans"><b>Generos:</b> '.$generos.'</p>';
				}
				
				$html.= '				<a class="waves-effect waves-light btn-large dull white-text opensans bold" id="backModal">';
				$html.= '					<i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Ir a VOD';
				$html.= '				</a>';
				
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '		<div class="row">';
				$html.= '			<div class="col s12 m12 l12">';
				$html.= '				<div class="boton-social-media">';
				$html.= '					<button class="social-m-b opensans" type="button"><i class="fa fa-plus fa-2x" aria-hidden="true"></i></button><br /><span>Mi Lista</span>';
				$html.= '				</div>';
				$html.= '				<div class="boton-social-media">';
				$html.= '					<button class="social-m-b opensans" type="button"><i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true"></i></button><br /><span>Calificar</span>';
				$html.= '				</div>';
				$html.= '				<div class="boton-social-media">';
				$html.= '					<button class="social-m-b opensans" type="button"><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i></button><br /><span>Facebook</span>';
				$html.= '				</div>';
				$html.= '				<div class="boton-social-media">';
				$html.= '					<button class="social-m-b opensans" type="button"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></button><br /><span>Twitter</span>';
				$html.= '				</div>';
				
				if ($itunes)
				{
					$html.= '				<div class="boton-social-media itunes-btn">';
					$html.= '					<button class="social-m-b opensans" type="button"><i class="fa fa-apple fa-2x" aria-hidden="true"></i></button><br /><span>iTunes</span>';
					$html.= '				</div>';
				}
				
				if ($plataforma)
				{
					$html.= '				<div class="boton-social-media plataforma-btn">';
					$html.= '					<button class="social-m-b opensans" type="button"><i class="fa fa-play-circle fa-2x" aria-hidden="true"></i></button><br /><span>Plataforma Canal Once</span>';
					$html.= '				</div>';
				}
				
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '	</div>';
				$html.= '</div>';
				
				if ($contenido) {
					
					$html.= '<ul class="collapsible" data-collapsible="accordion">';
					$contador_item = 0;
					
					foreach ($contenido as $item)
					{
						$contador_item++;
						
						$html.= '	<li>';
						if ($contador_item != 1)
						{
							$html.= '		<div class="collapsible-header dull white-text uppercase opensans bold"><i class="material-icons">video_library</i>'.$item['nombre'].'<span class="hide-on-small-only right">('.count($item['videos']).' videos)</span></div>';
						}
						else
						{
							$html.= '		<div class="collapsible-header dull white-text uppercase opensans bold active"><i class="material-icons">video_library</i>'.$item['nombre'].'<span class="hide-on-small-only right">('.count($item['videos']).' videos)</span></div>';
						}
						$html.= '		<div class="collapsible-body">';
						
						foreach ($item['videos'] as $video)
						{
							$lanzamiento = get_field("lanzamiento",$video->ID);
							$youtube_id = get_field("youtube_id",$video->ID);
							$portada = get_field("portada",$video->ID);
							
							$year = substr($lanzamiento, 0, 4);
							$month = substr($lanzamiento, 4, 2);
							$day = substr($lanzamiento, 6, 2);
							
							$html.= '	<a href="'.get_permalink($video->ID).'">';
							$html.= '		<div class="row">';
							$html.= '			<div class="col s12 m3 l3">';
							$html.= '				<img class="imagen-episodio responsive-img" src="https://img.youtube.com/vi/'.$youtube_id.'/mqdefault.jpg">';
							$html.= '			</div>';
							$html.= '			<div class="col s12 m9 l9">';
							$html.= '				<div class="titulo-episodio">';
							$html.= '					<h4 class="opensans white-text titulo-collapsible">'.$video->post_title.'</h4>';
							$html.= '				</div>';
							$html.= '				<div class="descripcion-episodio">';
							$html.= '					<p class="opensans white-text descripcion-collapsible">'.$video->post_content.'</p>';
							$html.= '				</div>';
							$html.= '			</div>';
							$html.= '		</div>';
							$html.= '	</a>';
						}
						
						$html.= '		</div>';
						$html.= '	</li>';
					}
					
					$html.= '</ul>';
				}
				
				//Show Results
				$array = array(
					'status' => (int)1,
					'msg' => (string)'success',
					'data' => $result,
					'html' => $html
				);
				
				//Print JSON Array
				printJSON($array);
				$output = TRUE;	
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No se pudo procesar la información en este momento.'
				);
		
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//getProgramInfo
		if ($msg == 'getProgramInfo')
		{
			//Leemos los Valores
			$ID = (isset($fields['ID'])) ? (string)trim($fields['ID']) : '';
			$container = (isset($fields['container'])) ? (string)trim($fields['container']) : '';
			
			//Verificamos Valores
			if ($ID)
			{
				//Leer el Video
				$programa = get_post($ID);
				
				//Procesamos las Variables
				$itunes = get_field("itunes", $programa->ID);
				$plataforma = get_field("plataforma", $programa->ID);
				$contenido = get_field("contenido",$programa->ID );
				$logo = get_field("logo",$programa->ID);
				$portada = get_field("portada",$programa->ID);
				$info = get_field("info",$programa->ID);
				$excerpt = $programa->post_excerpt;
				if (!$excerpt) { $excerpt = $programa->post_content; }
				
				//Generos
				$array_generos = get_the_terms($programa->ID, 'tematica'); $generos = '';
				if (is_array($array_generos))
				{
					foreach ($array_generos as $genero)
					{
						if ($generos) { $generos = $generos . ', ' . $genero->name; }
						else { $generos = $genero->name; }
					}
				}
				
				//Generamos el Arreglo
				$result = array(
					'ID' => $programa->ID,
					'title' => $programa->post_title,
					'content' => $programa->post_content,
					'genres' => $generos,
					'videos' => $contenido
				);
				
				//Obtenemos la URL del Video Siguiente
				$siguiente = '';
				if ($contenido) 
				{
					foreach ($contenido as $item)
					{
						foreach ($item['videos'] as $video)
						{
							$siguiente = get_permalink($video->ID);
							break;
						}
					}
				}
				
				//Generamos el HTML
				$html = '';
				$html.= '<div class="col s12 m12 l12 no-padding edit-img-series" style="background-image: url('.$portada.');">';
				$html.= '	<div class="col s12 m12 l12 sombra-arriba"></div>';
				$html.= '	<div class="col s12 m12 l12 sombra-contenedor"></div>';
				$html.= '	<div class="col s12 m12 l12 sombra-abajo"></div>';
				$html.= '	<div class="col s12 m12 l12 padding-left-30-desktop" style="position: relative; z-index: 10;">';
				$html.= '		<button class="btn-close-info" container="'.$container.'" type="button">';
				$html.= '			<i class="material-icons white-text" style="font-size: 3rem;">highlight_off</i>';
				$html.= '		</button>';
				$html.= '		<div class="space40"></div>';
				$html.= '		<div class="col s12 m12 l6">';
				$html.= '			<div class="contenedor-logo-serie">';
				if ($info)
				{
					$html.= '				<img class="responsive-img" src="'.$info.'">';
				}
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '		<div class="col s12 m12 l12">';
				$html.= '			<p class="roboto font18 white-text">';
				$html.= '				Categorías: <span class="roboto font18 white-text uppercase">'.$generos.'</span>';
				$html.= '			</p>';
				$html.= '		</div>';
				$html.= '		<div class="col s12 m12 l6">';
				$html.= '			<div class="col s6 m4 offset-m2 l4" style="padding: 0 2px;">';
				
				if ($siguiente)
				{
					$html.= '				<a href="'.$siguiente.'">';
					$html.= '					<div class="contenedor-reprodicir-slider centered">';
					$html.= '						<img class="" style="width: 32px; height: 32px;" src="'.get_bloginfo("template_directory").'/img/img_player.png">';
					$html.= '						<span class="roboto font22 gray-text">Reproducir</span>';
					$html.= '					</div>';
					$html.= '				</a>';
				}
					
				$html.= '			</div>';
				$html.= '			<div class="col s6 m4 l4" style="padding: 0 2px;">';
				
				if ($contenido)
				{
					$html.= '				<div class="contenedor-reprodicir-slider centered">';
				
				
					$html.= '					<a class="dropdown-button btn-temporadas-slider btn font22 roboto no-padding gray-text" data-activates="contenido_'.$programa->ID.'">Contenido<i class="small material-icons">arrow_drop_down</i></a>';
					$html.= '					<ul id="contenido_'.$programa->ID.'" class="dropdown-content black">';
					
					foreach ($contenido as $item)
					{
						$html.= '						<li><a class="white-text" href="'.get_permalink($programa->ID).'?content='.sanitize_title($item['nombre']).'">'.$item['nombre'].'</a></li>';
					}
				
					$html.= '					</ul>';
					$html.= '				</div>';
				}
				
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '		<div class="col s12 m12 l12">';
				$html.= '			<div class="col s12 m12 l6 no-padding">';
				$html.= '				<div class="space30"></div>';
				$html.= '				<p class="roboto font18 white-text mobil-text-justify">'.$excerpt.'</p>';
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '		<div class="col s12 m12 l6">';
				$html.= '			<div class="space20"></div>';
				$html.= '			<div class="col s6 m4 offset-m2 l4" style="padding: 0 2px;">';
				$html.= '				<a href="'.get_permalink($programa->ID).'">';
				$html.= '					<div class="contenedor-btn-mas centered">';
				$html.= '						<span class="roboto font18 white-text">VER MÁS</span>';
				$html.= '					</div>';
				$html.= '				</a>';
				$html.= '				<div class="space40"></div>';
				$html.= '			</div>';
				$html.= '			<div class="col s6 m4 l4" style="padding: 0 2px;">';
				$html.= '				<a href="#">';
				$html.= '					<div class="contenedor-btn-favorito centered" rel="'.$programa->ID.'">';
				$html.= '						<i class="material-icons white-text btn-fav-img-serie">add_circle_outline</i>';
				$html.= '						<span class="roboto font14 white-text uppercase">FAVORITOS</span>';
				$html.= '					</div>';
				$html.= '				</a>';
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '	</div>';
				$html.= '</div>';
				
				//Show Results
				$array = array(
					'status' => (int)1,
					'msg' => (string)'success',
					'data' => $result,
					'html' => $html
				);
				
				//Print JSON Array
				printJSON($array);
				$output = TRUE;	
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No se pudo procesar la información en este momento.'
				);
		
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//sendDemo
		if ($msg == 'sendDemo')
		{
			$to = 'milio.hernandez@gmail.com';
			$subject = 'Test Email';
			$body = 'Test Content';
			$headers = array('Content-Type: text/html; charset=UTF-8');
			$status = 'fail';
			 
			if (wp_mail( $to, $subject, $body, $headers ))
			{
				$status = 'send';
			}
			
			//Show Results
			$array = array(
				'status' => (int)1,
				'msg' => (string)'success',
				'data' => $status
			);
			
			//Print JSON Array
			printJSON($array);
			$output = TRUE;	
		}
		
		//addFavorite
		if ($msg == 'addFavorite')
		{
			//Leemos los Datos
			$usuario = (isset($fields['usuario'])) ? (string)trim($fields['usuario']) : '';
			$programa = (isset($fields['programa'])) ? (string)trim($fields['programa']) : '';
			
			//Verificamos Valores
			if ($usuario && $programa)
			{
				//Consultamos la Relación
				$args = array(
					'post_type' => 'favorito',
					'posts_per_page' => -1,
					'order' => 'ASC',
					'orderby' => 'title',
					'meta_query' => array(
						'relation' => 'AND',
						array(
							'key' => 'usuario',
							'value' => $usuario,
							'compare' => '='
						),
						array(
							'key' => 'programa',
							'value' => $programa,
							'compare' => '='
						)
					)
				); 
				$query = new WP_Query( $args );
				
				//Check Relationship
				if (count($query->posts) == 0)
				{
					//Register Coach
					$my_post = array(
					  'post_title'    => wp_strip_all_tags($usuario . ' - ' . $programa, true),
					  'post_status'   => 'publish',
					  'post_author'   => 1,
					  'post_type'	  => 'favorito'
					);

					// Save Data
					$post_id = wp_insert_post( $my_post );

					//Verify
					if ($post_id != 0)
					{
						// Save Custom Fields
						if ( ! update_post_meta ($post_id, 'usuario', $usuario ) ) add_post_meta( $post_id, 'usuario', $usuario );
						if ( ! update_post_meta ($post_id, 'programa', $programa ) ) add_post_meta( $post_id, 'programa', $programa );
					}
					
					//Build Response Array
					$array = array(
						'status' => (int)1,
						'msg' => 'success'
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este programa ya esta en tu lista de Favoritos.',
						'data' => array()
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//createUser
		if ($msg == 'createUser')
		{
			//Read Data
			$nombre = (isset($fields['nombre'])) ? (string)trim($fields['nombre']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			$genero = (isset($fields['genero'])) ? (string)trim($fields['genero']) : '';
			$fecha = (isset($fields['fecha'])) ? (string)trim($fields['fecha']) : '';
			$tematicas = (isset($fields['tematicas'])) ? (string)trim($fields['tematicas']) : ''; 
			$notificacion = (isset($fields['notificacion'])) ? (string)trim($fields['notificacion']) : '0';
			$array_tematicas = explode(',', $tematicas);
			
			//Check Values
			if ($nombre && $email && $password && $genero && $fecha && $fecha)
			{
				//Query User
				$args = array(
					'post_type' => 'usuario',
					'meta_query' => array(
						array(
							'key' => 'email',
							'value' => $email,
							'compare' => '='
						)
					)
				);
				$query = get_posts($args);
				
				//Check User exists
				if (count($query) == 0)
				{
					//Register Coach
					$my_post = array(
					  'post_title'    => wp_strip_all_tags($nombre, true),
					  'post_status'   => 'publish',
					  'post_author'   => 1,
					  'post_type'	  => 'usuario'
					);

					// Save Data
					$post_id = wp_insert_post( $my_post );

					//Verify
					if ($post_id != 0)
					{
						// Save Custom Fields
						if ( ! update_post_meta ($post_id, 'nombre', $nombre ) ) add_post_meta( $post_id, 'nombre', $nombre );
						if ( ! update_post_meta ($post_id, 'password', sha1($password) ) ) add_post_meta( $post_id, 'password', sha1($password) );
						if ( ! update_post_meta ($post_id, 'email', $email ) ) add_post_meta( $post_id, 'email', $email );
						if ( ! update_post_meta ($post_id, 'genero', $genero ) ) add_post_meta( $post_id, 'genero', $genero );
						if ( ! update_post_meta ($post_id, 'fecha', $fecha ) ) add_post_meta( $post_id, 'fecha', $fecha );
						if ( ! update_post_meta ($post_id, 'newsletter', $notificacion ) ) add_post_meta( $post_id, 'newsletter', $notificacion );
					}
					
					$field_key = "field_5a0b1260ec369";
					$value = get_field($field_key, $post_id);
					foreach ($array_tematicas as $tematica)
					{
						if ($tematica != true || $tematica != false)
						{
							$term = get_term_by('id', (int)$tematica, 'tematica');
							if ($term)
							{
								$value[] = array("nombre" => $term->name);
							}
						}	
					}
					update_field( $field_key, $value, $post_id );
					
					//Create User Session
					$array_user = array(
						'id' => $post_id,
						'nombre' => get_post_meta($post_id,'nombre',true),
						'email' => get_post_meta($post_id,'email',true)
					);
					
					//Set User Session
					$wp_session['user'] = $array_user;
					$wp_session['logged'] = TRUE;
					
					//Mail Notification
					$subject = 'Bienvenido a Video On Demand de Canal Once';
					$template = wp_remote_get( esc_url_raw (get_bloginfo('url').'/mail-welcome/?param01='.$nombre.'&param02='.$password.'&param03='.$email.'&param04='.$genero.'&param05='.$fecha) );
					$body = $template['body'];
					$altbody = strip_tags($body);

					//Check Email Data
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Send Email
						add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
						$response = wp_mail( $email, $subject, $body );
					}
					
					//Build Response Array
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $array_user
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este usuario ya está registrado. Inicia Sesión.',
						'data' => array()
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//doLogin
		if ($msg == 'doLogin')
		{
			//Read Data
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			
			//Check Values
			if ($email && $password)
			{
				//Query User
				$args = array(
					'post_type' => 'usuario',
					'meta_query' => array(
						'relation' => 'AND',
						array(
							'key' => 'email',
							'value' => $email,
							'compare' => '='
						)
					)
				);
				$query = new WP_Query( $args );
				
				//Check User exists
				if (count($query->posts) > 0)
				{
					//Read Object
					foreach ($query->posts as $row) { $post_id = $row->ID; }
					
					//Verificamos si el Password coincide
					if (sha1($password) == get_field("password", $post_id))
					{
						//Create User Session
						$array_user = array(
							'id' => $post_id,
							'nombre' => get_post_meta($post_id,'nombre',true),
							'email' => get_post_meta($post_id,'email',true)
						);
						
						//Set User Session
						$wp_session['user'] = $array_user;
						$wp_session['logged'] = TRUE;
						
						//Build Response Array
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $array_user
						);
	
						//Print JSON Array
						printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Show Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'La contraseña es incorrecta. Revísala e intenta de nuevo.',
							'data' => array()
						);
		
						//Print JSON Array
						printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'El usuario no existe. Crea una cuenta.',
						'data' => array()
					);
	
					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//doForgot
		if ($msg == 'doForgot')
		{
			//Read Data
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			
			//Check Values
			if ($email)
			{
				//Query User
				$args = array(
					'post_type' => 'usuario',
					'meta_query' => array(
						array(
							'key' => 'email',
							'value' => $email,
							'compare' => '='
						)
					)
				);
				$query = new WP_Query( $args );
				
				//Check User exists
				if (count($query->posts) > 0)
				{
					//Read Object
					foreach ($query->posts as $row) { $post_id = $row->ID; }
					
					//Creamos un Token
					$token = sha1(current_time( 'timestamp' ));
					if ( ! update_post_meta ($post_id, 'token', $token ) ) add_post_meta( $post_id, 'token', $token );
					
					//Generamos el Link
					$nombre = get_field("nombre",$post_id);
					
					//Mail Notification
					$subject = 'Recuperar Contraseña Video On Demand de Canal Once';
					$template = wp_remote_get( esc_url_raw (get_bloginfo('url').'/mail-forgot/?param01='.$nombre.'&param02='.$email.'&param03='.$token) );
					$body = $template['body'];
					$altbody = strip_tags($body);

					//Check Email Data
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Send Email
						add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
						$response = wp_mail( $email, $subject, $body );
					}
					
					//Build Response Array
					$array = array(
						'status' => (int)1,
						'msg' => 'success'
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'El usuario no existe. Crea una cuenta.',
						'data' => array()
					);
	
					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//doReset
		if ($msg == 'doReset')
		{
			//Read Data
			$nickname = (isset($fields['nickname'])) ? (string)trim($fields['nickname']) : '';
			$token = (isset($fields['token'])) ? (string)trim($fields['token']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			
			//Check Values
			if ($nickname && $password && $token)
			{
				//Verificamos que el Usuario exista y le corresponda el Token
				$args = array(
					'post_type' => 'usuario',
					'posts_per_page' => 1,
					'meta_query' => array(
						'relation' => 'AND',
						array(
							'key' => 'nombre',
							'value' => $nickname,
							'compare' => '='
						),
						array(
							'key' => 'token',
							'value' => $token,
							'compare' => '='
						),
					)
				);
				$query = new WP_Query( $args );
	
				//Check User exists
				if (count($query->posts) > 0)
				{
					//Read Object
					foreach ($query->posts as $row) { $post_id = $row->ID; }
					
					//Actualizamos el Password
					if ( ! update_post_meta ($post_id, 'password', sha1($password) ) ) add_post_meta( $post_id, 'password', sha1($password) );
					if ( ! update_post_meta ($post_id, 'token', '' ) ) add_post_meta( $post_id, 'token', '' );
					
					//Build Response Array
					$array = array(
						'status' => (int)1,
						'msg' => 'success'
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'El token y el usuario no coindicen. Inicia el proceso de recuperación de nuevo.',
						'data' => array()
					);
	
					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//updateVideoLog
		if ($msg == 'updateVideoLog')
		{
			//Leemos los Datos
			$video_id = (isset($fields['video_id'])) ? (string)trim($fields['video_id']) : '';
			$user_id = (isset($fields['user_id'])) ? (string)trim($fields['user_id']) : '';
			$action = (isset($fields['action'])) ? (string)trim($fields['action']) : '';
			
			//Verificamos
			if ( $video_id && $user_id && $action )
			{
				//Consultamos si existe el Video y el Usuario
				$video = get_post($video_id);
				$user = get_post($user_id);
				
				//Verificamos si existe el Video
				if ($video)
				{
					//Verificamos si existe el Usuario
					if ($user)
					{
						//Consultamos la Relación
						$args = array(
							'post_type' => 'log',
							'posts_per_page' => 1,
							'order' => 'ASC',
							'orderby' => 'title',
							'meta_query' => array(
								'relation' => 'AND',
								array(
									'key' => 'usuario',
									'value' => $user->ID,
									'compare' => '='
								),
								array(
									'key' => 'video',
									'value' => $video->ID,
									'compare' => '='
								)
							)
						); 
						$query = new WP_Query( $args );
						
						//Check Relationship
						if (count($query->posts) == 0)
						{
							//Register Coach
							$my_post = array(
							  'post_title'    => wp_strip_all_tags($user->ID . ' - ' . $video->ID, true),
							  'post_status'   => 'publish',
							  'post_author'   => 1,
							  'post_type'	  => 'log'
							);
		
							// Save Data
							$post_id = wp_insert_post( $my_post );
		
							//Verify
							if ($post_id != 0)
							{
								// Save Custom Fields
								if ( ! update_post_meta ($post_id, 'usuario', $user->ID ) ) add_post_meta( $post_id, 'usuario', $user->ID );
								if ( ! update_post_meta ($post_id, 'video', $video->ID ) ) add_post_meta( $post_id, 'video', $video->ID );
								if ( ! update_post_meta ($post_id, 'action', $action ) ) add_post_meta( $post_id, 'action', $action );
								if ( ! update_post_meta ($post_id, 'date', current_time( 'timestamp', 0 ) ) ) add_post_meta( $post_id, 'date', current_time( 'timestamp', 0 ) );
							}
							
							//Build Response Array
							$array = array(
								'status' => (int)1,
								'msg' => 'success'
							);
		
							//Print JSON Array
							printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Leemos el Resultado
							foreach ($query->posts as $log) { }
							$post_id = $log->ID;
							
							//Actualizamos los Datos
							if ( ! update_post_meta ($post_id, 'usuario', $user->ID ) ) add_post_meta( $post_id, 'usuario', $user->ID );
							if ( ! update_post_meta ($post_id, 'video', $video->ID ) ) add_post_meta( $post_id, 'video', $video->ID );
							if ( ! update_post_meta ($post_id, 'action', $action ) ) add_post_meta( $post_id, 'action', $action );
							if ( ! update_post_meta ($post_id, 'date', current_time( 'timestamp', 0 ) ) ) add_post_meta( $post_id, 'date', current_time( 'timestamp', 0 ) );
							
							
							//Build Response Array
							$array = array(
								'status' => (int)1,
								'msg' => 'update'
							);
		
							//Print JSON Array
							printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Show Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'El usuario no existe.',
							'data' => array()
						);
		
						//Print JSON Array
						printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'El video no existe.',
						'data' => array()
					);
	
					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//getProgramInfo
		if ($msg == 'getProgramInfoV2')
		{
			//Leemos los Valores
			$ID = (isset($fields['ID'])) ? (string)trim($fields['ID']) : '';
			$container = (isset($fields['container'])) ? (string)trim($fields['container']) : '';
			
			//Verificamos Valores
			if ($ID)
			{
				//Leer el Video
				$programa = get_post($ID);
				
				//Procesamos las Variables
				$itunes = get_field("itunes", $programa->ID);
				$plataforma = get_field("plataforma", $programa->ID);
				$contenido = get_field("contenido",$programa->ID );
				$logo = get_field("logo",$programa->ID);
				$portada = get_field("portada",$programa->ID);
				$info = get_field("info",$programa->ID);
				$excerpt = $programa->post_excerpt;
				if (!$excerpt) { $excerpt = $programa->post_content; }
				
				//Generos
				$array_generos = get_the_terms($programa->ID, 'tematica'); $generos = '';
				if (is_array($array_generos))
				{
					foreach ($array_generos as $genero)
					{
						if ($generos) { $generos = $generos . ', ' . $genero->name; }
						else { $generos = $genero->name; }
					}
				}
				
				//Generamos el Arreglo
				$result = array(
					'ID' => $programa->ID,
					'title' => $programa->post_title,
					'content' => $programa->post_content,
					'genres' => $generos,
					'videos' => $contenido
				);
				
				//Obtenemos la URL del Video Siguiente
				$siguiente = '';
				if ($contenido) 
				{
					foreach ($contenido as $item)
					{
						foreach ($item['videos'] as $video)
						{
							$siguiente = get_permalink($video->ID);
							break;
						}
					}
				}
				
				//Generamos el HTML
				$html = '';
				$html.= '<div class="col s12 m12 l12 no-padding edit-img-series" style="background-image: url('.$portada.');">';
				$html.= '	<div class="col s12 m12 l12 sombra-arriba"></div>';
				$html.= '	<div class="col s12 m12 l12 sombra-contenedor"></div>';
				$html.= '	<div class="col s12 m12 l12 sombra-abajo"></div>';
				$html.= '	<div class="col s12 m12 l12 padding-left-30-desktop" style="position: relative; z-index: 10;">';
				$html.= '		<button class="btn-close-info" container="'.$container.'" type="button">';
				$html.= '			<i class="material-icons white-text" style="font-size: 3rem;">highlight_off</i>';
				$html.= '		</button>';
				$html.= '		<div class="space40"></div>';
				$html.= '		<div class="col s12 m12 l6">';
				$html.= '			<div class="contenedor-logo-serie">';
				if ($info)
				{
					$html.= '				<img class="responsive-img" src="'.$info.'">';
				}
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '		<div class="col s12 m12 l12">';
				$html.= '			<p class="roboto font18 white-text">';
				$html.= '				Categorías: <span class="roboto font18 white-text uppercase">'.$generos.'</span>';
				$html.= '			</p>';
				$html.= '		</div>';
				$html.= '		<div class="col s12 m12 l6">';
				$html.= '			<div class="col s6 m4 offset-m2 l4" style="padding: 0 2px;">';
				
				if ($siguiente)
				{
					$html.= '				<a href="'.$siguiente.'">';
					$html.= '					<div class="contenedor-reprodicir-slider centered">';
					$html.= '						<img class="" style="width: 32px; height: 32px;" src="'.get_bloginfo("template_directory").'/img/img_player.png">';
					$html.= '						<span class="roboto font22 gray-text">Reproducir</span>';
					$html.= '					</div>';
					$html.= '				</a>';
				}
					
				$html.= '			</div>';
				$html.= '			<div class="col s6 m4 l4" style="padding: 0 2px;">';
				
				if ($contenido)
				{
					$html.= '				<div class="contenedor-reprodicir-slider centered">';
				
				
					$html.= '					<a class="dropdown-button btn-temporadas-slider btn font22 roboto no-padding gray-text" data-activates="contenido_'.$programa->ID.'">Contenido<i class="small material-icons">arrow_drop_down</i></a>';
					$html.= '					<ul id="contenido_'.$programa->ID.'" class="dropdown-content black">';
					
					foreach ($contenido as $item)
					{
						$html.= '						<li><a class="white-text" href="'.get_bloginfo("url").'/v2/programa/?id='.$programa->ID.'&content='.sanitize_title($item['nombre']).'">'.$item['nombre'].'</a></li>';
					}
				
					$html.= '					</ul>';
					$html.= '				</div>';
				}
				
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '		<div class="col s12 m12 l12">';
				$html.= '			<div class="col s12 m12 l6 no-padding">';
				$html.= '				<div class="space30"></div>';
				$html.= '				<p class="roboto font18 white-text mobil-text-justify">'.$excerpt.'</p>';
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '		<div class="col s12 m12 l6">';
				$html.= '			<div class="space20"></div>';
				$html.= '			<div class="col s6 m4 offset-m2 l4" style="padding: 0 2px;">';
				$html.= '				<a href="'.get_bloginfo("url").'/v2/programa/?id='.$programa->ID.'">';
				$html.= '					<div class="contenedor-btn-mas centered">';
				$html.= '						<span class="roboto font18 white-text">VER MÁS</span>';
				$html.= '					</div>';
				$html.= '				</a>';
				$html.= '				<div class="space40"></div>';
				$html.= '			</div>';
				$html.= '			<div class="col s6 m4 l4" style="padding: 0 2px;">';
				$html.= '				<a href="#">';
				$html.= '					<div class="contenedor-btn-favorito centered" rel="'.$programa->ID.'">';
				$html.= '						<i class="material-icons white-text btn-fav-img-serie">add_circle_outline</i>';
				$html.= '						<span class="roboto font14 white-text uppercase">FAVORITOS</span>';
				$html.= '					</div>';
				$html.= '				</a>';
				$html.= '			</div>';
				$html.= '		</div>';
				$html.= '	</div>';
				$html.= '</div>';
				
				//Show Results
				$array = array(
					'status' => (int)1,
					'msg' => (string)'success',
					'data' => $result,
					'html' => $html
				);
				
				//Print JSON Array
				printJSON($array);
				$output = TRUE;	
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No se pudo procesar la información en este momento.'
				);
		
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//getContentProgram
		if ($msg == 'getContentProgram')
		{
			//Leer los datos
			$ID = (isset($fields['ID'])) ? (string)trim($fields['ID']) : '';
			$content = (isset($fields['content'])) ? (string)trim($fields['content']) : '';
			$order = (isset($fields['order'])) ? (string)trim($fields['order']) : '';
			
			//Verificamos
			if ($ID && $content && $order)
			{
				//Leemos el Programa
				$programa = get_post($ID);
				$contenido_programa = array();
				$contador_contenido_programa = 0;
				$html = '';
				
				//Verificamos
				if ($programa)
				{
					//Leemos el contenido del programa
					$contenido = get_field("contenido", $programa->ID );
					
					//Consultamos el contenido
					foreach ($contenido as $item)
					{
						$contador_contenido_programa++;
						if (sanitize_title($item['nombre']) == $content)
						{
							$contenido_programa = $item;
							break;
						}
						
						if (count($contenido) == $contador_contenido_programa)
						{
							$contenido_programa = $item;
							break;
						}
					}
					
					//Verificamos el Orden
					if ($order == 'DESC') 
					{ 
						$videos = $contenido_programa['videos'];
						$videos = array_reverse($videos);
						$contenido_programa['videos'] = $videos;
					}
					
					//Procesamos el Contenido
					$html.= '<div class="col s12 m10 offset-m1 l12" style="position: relative; padding-bottom: 45px;">';
					$html.= '	<a href="#">';
					$html.= '		<div class="btn-arriba">';
					$html.= '			<i class="material-icons white-text" style="font-size: 3rem;">expand_less</i>';
					$html.= '		</div>';
					$html.= '	</a>';
					
					//Procesamos los Videos
					foreach ($contenido_programa['videos'] as $video)
					{
						$excerpt_video = $video->post_excerpt;
						if (!$excerpt_video) { $excerpt_video = $video->post_content; }
						
						$html.= '	<a href="'.get_permalink($video->ID).'">';
						$html.= '		<div class="space40"></div>';
						$html.= '			<div class="row">';
						$html.= '				<div class="col s6 m4 l3">';
						$html.= '					<div class="contenedor-reproducir-episodio-serie">';
						$html.= '						<img class="btn-play-serie" src="'.get_bloginfo("template_directory").'/img/img_episodios_cenado_btn_play.png">';
						$html.= '						<img class="responsive-img-full-w-h" src="https://img.youtube.com/vi/'.get_field("youtube_id", $video->ID).'/mqdefault.jpg">';
						$html.= '					</div>';
						$html.= '				</div>';
						$html.= '				<div class="col s12 m8 l9">';
						$html.= '					<span class="titulo-episodio roboto bold font23 white-text block">'.$video->post_title.'</span>';
						$html.= '					<span class="descripcion-episodio roboto font16 white-text">'.$excerpt_video.'</span>';
						$html.= '				<div class="space40"></div>';
						$html.= '			</div>';
						$html.= '		</div>';
						$html.= '	</a>';
					}
					
					$html.='</div>';
					
					//Show Results
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success',
						'data' => $contenido_programa,
						'html' => $html
					);
					
					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe el programa.'
					);
			
					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No se pudo procesar la información en este momento.'
				);
		
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
	}
	else
	{
		//Show Error
		$array = array(
			'status' => (int)0,
			'msg' => (string)'API Call Invalid.'
		);

		//Print JSON Array
		printJSON($array);
		$output = TRUE;
	}

	//Check Output
	if (!$output)
	{
		//Show Error
		$array = array(
			'status' => (int)0,
			'msg' => (string)'API Error.'
		);

		//Print JSON Array
		printJSON($array);
		$output = TRUE;
	}
	
?>