<?php get_header(); ?>

<div class="row info-serie">
	<div class="col s12 m12 l12 no-padding img-banner-serie">
		<div class="col s12 m12 l12 sombra-arriba hide-on-med-and-down">
				
		</div>
		<div class="col s12 m12 l12 sombra-contenedor hide-on-med-and-down">
			
		</div>
		<div class="col s12 m12 l12 sombra-abajo-serie">
			
		</div>
		<div class="col s12 m12 l12 hide-on-large-only">
			<div class="contenedor-banner-img-mobile-serie">
				<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_cenado.png">
			</div>
		</div>
		<div class="col s12 m12 l12 hide-on-med-and-down" style="position: relative; z-index: 10;">
			<div class="space40"></div>
			<div class="col s12 m12 l12 no-padding">
				<div class="contenedor-logo-serie margin-100-serie">
					<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/logo_cenado.png">
				</div>
			</div>
			<div class="col s12 m12 l6">
				<div class="space20"></div>
				<div class="col s6 m4 offset-m2 l4 offset-l2" style="padding: 0 2px;">
					<a href="#">
						<div class="contenedor-reprodicir-slider centered">
							<img class="" style="width: 32px; height: 32px;" src="../wp-content/themes/vod_v2/img/img_player.png">
							<span class="roboto font22 gray-text">Reproducir</span>
						</div>
					</a>
				</div>
				<div class="col s6 m4 l4" style="padding: 0 2px;">
					<a href="#">
						<div class="contenedor-btn-favorito centered">
							<i class="material-icons white-text btn-fav-img-serie">add_circle_outline</i>
							<span class="roboto font14 white-text uppercase">FAVORITOS</span>
						</div>
					</a>
				</div>
			</div>
			<div class="col s12 m12 l12">
				<div class="col s12 m12 l5 offset-l1 no-padding">
					<div class="space30"></div>
					<p class="roboto font18 white-text mobil-text-justify">
						Bruno Bichir, a manera de gourmet filosófico, visita diferentes lugares de la república mexicana en busca de la magia, los sabores y las historias que se ocultan detrás de los platillos que encuentra a su paso.
					</p>
				</div>
			</div>
			<div class="col s12 m12 l11 offset-l1">
				<p class="roboto font18 white-text">
					Genero: <span class="roboto font18 white-text uppercase">Cocina</span>
				</p>
			</div>
		</div>
	</div>
</div>

<!--Programacion y Buscador-->
<div class="container-fluid" style="background-color: #000;">
	<div class="row" style="margin-bottom: 0;">
		<div class="col l5 hide-on-med-and-down">
			<div class="col s12 m12 l7 no-padding centered" style="line-height: 70px">
				<!-- Dropdown Trigger -->
				<a class='dropdown-button btn font22 roboto black no-padding' href='#' data-activates='programas1'>PROGRAMAS A - Z<i class="small material-icons">arrow_drop_down</i></a>
				
				<!-- Dropdown Structure -->
				<ul id='programas1' class='dropdown-content'>
					<li><a href="#!">three</a></li>
					<li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
					<li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
				</ul>
			</div>
			<div class="col s12 m12 l5 no-padding centered" style="line-height: 70px;">
				<!-- Dropdown Trigger -->
				<a class='dropdown-button btn font22 roboto black uppercase no-padding' href='#' data-activates='categorias1'>categorías<i class="small material-icons">arrow_drop_down</i></a>
				
				<!-- Dropdown Structure -->
				<ul id='categorias1' class='dropdown-content'>
					<li><a href="#!">one</a></li>
					<li><a href="#!">two</a></li>
					<li class="divider"></li>
					<li><a href="#!">three</a></li>
				</ul>
			</div>
		</div>
		<div class="col s10 m11 l6">
			<form>
				<div class="input-field">
					<input id="search" class="editar-busqueda" type="search" placeholder="Introduce el nombre del programa que deseas buscar" required>
					<label class="label-icon" for="search"><i class="material-icons"></i></label>
					<i class="material-icons">close</i>
				</div>
			</form>
		</div>
		<div class="col s2 m1 l1">
			<i class="material-icons white-text icon-busqueda">search</i></label>
		</div>
	</div>
</div>

<div class="container-fluid episodios-series">
	<div class="row no-margin-row">
		<div class="col s12 m12 l4 hide-on-med-and-down">
			<div class="cont-img-serie">
				<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_cenado.png">
			</div>
		</div>
		<div class="col s12 m12 l8">
			<div class="space20"></div>
			<div class="col s12 m6 l6">
				<div class="col s6 m6 l5 hide-on-large-only" style="padding: 0 2px;">
					<a href="#">
						<div class="contenedor-reprodicir-slider centered">
							<img class="" style="width: 32px; height: 32px;" src="../wp-content/themes/vod_v2/img/img_player.png">
							<span class="roboto font22 gray-text">Reproducir</span>
						</div>
					</a>
				</div>
				<div class="col s6 m6 l8" style="padding: 0 2px;">
					<div class="contenedor-reprodicir-slider centered">
						<!-- Dropdown Trigger -->
						<a class='dropdown-button btn-temporadas-slider btn font22 roboto no-padding gray-text' href='#' data-activates='temporadas1'>Temporadas<i class="small material-icons">arrow_drop_down</i></a>
						
						<!-- Dropdown Structure -->
						<ul id='temporadas1' class='dropdown-content'>
							<li><a href="#!">three</a></li>
							<li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
							<li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l6">
				<div class="space10 hide-on-med-and-up"></div>
				<div class="col s6 m6 l5 hide-on-large-only">
					<a href="#">
						<div class="contenedor-btn-favorito centered">
							<i class="material-icons white-text btn-fav-img-serie">add_circle_outline</i>
							<span class="roboto font22 white-text uppercase">FAVORITOS</span>
						</div>
					</a>
				</div>
				<div class="col s6 m6 l8 float-right-serie" style="padding: 0 2px;">
					<a href="#">
						<div class="contenedor-reprodicir-slider centered">
							<span class="roboto font22 gray-text">Volver al Inicio</span>
						</div>
					</a>
				</div>
			</div>
			<div class="col s12 m12 l12">
				<div class="space20"></div>
				<span class="roboto bold white-text font35 uppercase">TERCERA TEMPORADA</span>
				<div class="space10"></div>
			</div>
			<div class="col s12 m10 offset-m1 l12" style="position: relative; padding-bottom: 45px;">
				<a href="#">
					<div class="btn-arriba">
						<i class="material-icons white-text" style="font-size: 3rem;">expand_less</i>
					</div>
				</a>
				<div class="space40"></div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_01.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">León, Guanajuato (EP 01)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno Bichir, visita la ciudad heredera de una antigua tradición artesanal,
						la capital mundial del calzado, León, Gto.
					</p>
					<div class="space40"></div>
				</div>
				<div class="space40"></div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_02.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Ciudad De México (EP 02)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno se prepara para disfrutar una de las más grandes metrópolis
						del mundo, la Ciudad de México.
					</p>
					<div class="space40"></div>
				</div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_03.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Zacatecas, Zacatecas (EP 03)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno visita la tierra minera por excelencia que en cada rincón muestra
						su pasado colonial.
					</p>
					<div class="space40"></div>
				</div>
				<div class="space40"></div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_04.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Huamantla, Tlaxcala (EP 04)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno visita el pueblo mágico de Huamantla, Tlaxcala, tierra de hombres
						y mujeres guerreros.
					</p>
					<div class="space40"></div>
				</div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_05.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Teul, Zacatecas (EP 05)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno recorre las calles de este bello y apacible lugar, el municipio de Teúl, ubicado en Zacatecas.
					</p>
					<div class="space40"></div>
				</div>
				<div class="space40"></div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_06.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Tlatlauquitepec, Puebla (EP 06)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno disfruta de la tranquilidad de las calles de Tlatlauquitepec,
						localizado en la Sierra Norte de Puebla.
					</p>
					<div class="space40"></div>
				</div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_07.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Jala, Nayarit (EP 07)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno visita el pueblo mágico de Jala en Nayarit, un pequeño municipio asentado en las faldas del volcán Ceboruco.
					</p>
					<div class="space40"></div>
				</div>
				<div class="space40"></div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_08.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Jerez, Zacatecas (EP 08)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno visita Jerez, pueblo de múltiples contrastes, tierra de migrantes,
						de gente hospitalaria, de comida deliciosa.
					</p>
					<div class="space40"></div>
				</div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_09.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Cadereyta, Querétaro (EP 09)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno recorre el pueblo mágico de Cadereyta en Querétaro,
						conocido como la tierra del nopal.
					</p>
					<div class="space40"></div>
				</div>
				<div class="space40"></div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_10.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Cuitzeo, Michoacán (EP 10)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno visita Cuitzeo, un lugar rodeado de una inmensa laguna,
						donde da un paseo en canoa y prueba la comida tradicional.
					</p>
					<div class="space40"></div>
				</div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_11.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Huichapan, Hidalgo (EP 11)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno visita una de las ciudades más bonitas de Hidalgo, por su clima,
						sus construcciones, su vegetación, Huichapan.
					</p>
					<div class="space40"></div>
				</div>
				<div class="space40"></div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_12.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Cuetzalan, Puebla (EP 12)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno descubre en Cuetzalan el sitio perfecto para conocer las tradiciones
						y rituales de la cultura totonaca.
					</p>
					<div class="space40"></div>
				</div>
				<div class="col s6 m4 l3">
					<div class="contenedor-reproducir-episodio-serie">
						<img class="btn-play-serie" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_btn_play.png">
						<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_episodios_cenado_13.png">
					</div>
				</div>
				<div class="col s12 m12 l9">
					<p class="titulo-episodio roboto bold font23 white-text">Altos De Jalisco, Jalisco (EP 13)</p>
					<p class="descripcion-episodio roboto font16 white-text">
						Bruno visita los Altos de Jalisco, una de las regiones criolla y castiza
						de México. Tierra del tequila y la charrería.
					</p>
					<div class="space40"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid" style="background-color: #440821;">
	<div class="row no-margin-row">
		<div class="contenedor-titulo-carrusel-interes">
			<span class="opns-bold-italic font22 green-text uppercase">
				también
			</span>
			<br>
			<span class="opns-bold-italic font38 white-text uppercase">
				te puede interesar
			</span>
			<div class="space20"></div>
		</div>
	</div>
	<div class="row carrusel-edit" style="margin-bottom: 0;">
		<a href="#">
			<img class="btn-left-carrusel" src="../wp-content/themes/vod_v2/img/img_left_arrow.png">
		</a>
		<div class="col s4 m4 l3 no-padding">
			<div class="contenedor-img-carrusel-interes">
				<a class="" href="#">
					<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_cocina.png">
				</a>
				<div class="info-text-carrusel-interes centered">
					<span class="opns-bold white-text uppercase">nueva temporada</span>
				</div>
			</div>
		</div>
		<div class="col s4 m4 l3 no-padding">
			<div class="contenedor-img-carrusel-interes">
				<a class="" href="#">
					<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_memorias.png">
				</a>
				<div class="info-text-carrusel-interes centered" style="display: none;">
					<span class="opns-bold white-text uppercase">nueva temporada</span>
				</div>
			</div>
		</div>
		<div class="col s4 m4 l3 no-padding">
			<div class="contenedor-img-carrusel-interes">
				<a href="#">
					<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_ruta.png">
				</a>
				<div class="info-text-carrusel-interes centered" style="display: none;">
					<span class="opns-bold white-text uppercase">ESTRENO</span>
				</div>
			</div>
		</div>
		<div class="col s4 m4 l3 no-padding hide-on-med-and-down">
			<div class="contenedor-img-carrusel-interes">
				<a href="#">
					<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_elogio.png">
				</a>
				<div class="info-text-carrusel-interes centered" style="display: none;">
					<span class="opns-bold white-text uppercase">ESTRENO</span>
				</div>
			</div>
		</div>
		<a href="#">
			<img class="btn-right-carrusel" src="../wp-content/themes/vod_v2/img/img_right_arrow.png">
		</a>
	</div>
</div>

<?php get_footer(); ?>