<?php get_header(); ?>

<!--Slider-->
<div class="container-fluid" style="background-color: #A7004C;">
	<div class="row" style="margin-bottom: 0;">
		<div class="col s12 m12 l12 no-padding">
			 <div class="slider categorias">
				<ul class="slides" style="position: relative;">
					<li>
						<img class="responsive-img-full-w-h show-on-large hide-on-med-and-down" src="../wp-content/themes/vod_v2/img/img_banner_categoria.png">
						<img class="responsive-img-full-w-h hide-on-large-only" src="../wp-content/themes/vod_v2/img/img_banner_categoria.png">
						<div class="caption">
							<div class="row no-margin-row-mobile">
								<div class="col s12 m6 l12">
									<div class="col s6 m6 l5" style="padding: 0 2px;">
										<a href="#">
											<div class="contenedor-reprodicir-slider centered">
												<img class="" style="width: 32px; height: 32px;" src="../wp-content/themes/vod_v2/img/img_player.png">
												<span class="roboto font22 gray-text">Reproducir</span>
											</div>
										</a>
									</div>
									<div class="col s6 m6 l5" style="padding: 0 2px;">
										<div class="contenedor-reprodicir-slider centered">
											<!-- Dropdown Trigger -->
											<a class='dropdown-button btn-temporadas-slider btn font22 roboto no-padding gray-text' href='#' data-activates='temporadas1'>Temporadas<i class="small material-icons">arrow_drop_down</i></a>
											
											<!-- Dropdown Structure -->
											<ul id='temporadas1' class='dropdown-content'>
												<li><a href="#!">three</a></li>
												<li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
												<li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col s12 m12 l12 hide-on-med-and-down">
									<p class="roboto font18">
										Un homenaje a la música popular, así como a los músicos, cantantes, compositores y arreglistas que dedicaron canciones al amor y al desamor.
									</p>
								</div>
								<div class="col s12 m6 l12">
									<div class="space10 hide-on-med-and-up"></div>
									<div class="col s6 m6 l4" style="padding: 0 2px;">
										<a href="#">
											<div class="contenedor-btn-mas centered">
												<span class="roboto font18 white-text">VER MÁS</span>
											</div>
											<div class="space10 hide-on-med-and-up"></div>
										</a>
									</div>
									<div class="col s6 m6 l5">
										<a href="#">
											<div class="contenedor-btn-favorito centered">
												<i class="material-icons white-text btn-fav-img">add_circle_outline</i>
												<span class="roboto font22 white-text uppercase btn-favoritos">FAVORITOS</span>
											</div>
											<div class="space10 hide-on-med-and-up"></div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!--Programacion y Buscador-->
<div class="container-fluid" style="background-color: #000;">
	<div class="row" style="margin-bottom: 0;">
		<div class="col l5 hide-on-med-and-down">
			<div class="col s12 m12 l7 no-padding centered" style="line-height: 70px">
				<!-- Dropdown Trigger -->
				<a class='dropdown-button btn font22 roboto black no-padding' href='#' data-activates='programas1'>PROGRAMAS A - Z<i class="small material-icons">arrow_drop_down</i></a>
				
				<!-- Dropdown Structure -->
				<ul id='programas1' class='dropdown-content'>
					<li><a href="#!">three</a></li>
					<li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
					<li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
				</ul>
			</div>
			<div class="col s12 m12 l5 no-padding centered" style="line-height: 70px;">
				<!-- Dropdown Trigger -->
				<a class='dropdown-button btn font22 roboto black uppercase no-padding' href='#' data-activates='categorias1'>categorías<i class="small material-icons">arrow_drop_down</i></a>
				
				<!-- Dropdown Structure -->
				<ul id='categorias1' class='dropdown-content'>
					<li><a href="#!">one</a></li>
					<li><a href="#!">two</a></li>
					<li class="divider"></li>
					<li><a href="#!">three</a></li>
				</ul>
			</div>
		</div>
		<div class="col s10 m11 l6">
			<form>
				<div class="input-field">
					<input id="search" class="editar-busqueda" type="search" placeholder="Introduce el nombre del programa que deseas buscar" required>
					<label class="label-icon" for="search"><i class="material-icons"></i></label>
					<i class="material-icons">close</i>
				</div>
			</form>
		</div>
		<div class="col s2 m1 l1">
			<i class="material-icons white-text icon-busqueda">search</i></label>
		</div>
	</div>
</div>

<!--Series Categoria-->
<div class="container-fluid categoria">
	<div class="row no-margin-row">
		<div class="col s12 m12 l12 padding-favoritos-series">
			<div class="space40"></div>
			<div class="contenedor-titulo-categorias">
				<span class="opns-bold-italic font36 white-text uppercase">
					CIENCIA Y TECNOLOGÍA
				</span>
				<div class="space20"></div>
			</div>
			<div class="space40"></div>
			<div class="col s12 m12 l12 contenedor-favoritos-series">
				<div class="col s12 m12 l12" style="padding: 0;">
					<div class="col s4 m4 l3" style="padding-left: 0;">
						<div class="contenedor-img-favoritos">
							<a href="#">
								<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_gaceta.png">
							</a>
						</div>
						<div class="space20"></div>
					</div>
					<div class="col s4 m4 l3" style="padding-left: 0;">
						<div class="contenedor-img-favoritos">
							<a href="#">
								<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_ideas.png">
							</a>
						</div>
						<div class="space20"></div>
					</div>
					<div class="col s4 m4 l3" style="padding-left: 0;">
						<div class="contenedor-img-favoritos">
							<a href="#">
								<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_encuentros.png">
							</a>
						</div>
						<div class="space20"></div>
					</div>
					<div class="col s4 m4 l3" style="padding-left: 0;">
						<div class="contenedor-img-favoritos">
							<a href="#">
								<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_casos.png">
							</a>
						</div>
						<div class="space20"></div>
					</div>
					<div class="col s4 m4 l3" style="padding-left: 0;">
						<div class="contenedor-img-favoritos">
							<a href="#">
								<img class="responsive-img-full-w-h" src="../wp-content/themes/vod_v2/img/img_carrusel_factor.png">
							</a>
						</div>
						<div class="space20"></div>
					</div>
				</div>
				<div class="col s12 m12 l12 no-padding">
				<div class="space40"></div>
				<div class="col s6 m4 l3" style="float: none; margin: 0 auto;">
					<div class="contenedor-reprodicir-slider centered">
						<span class="roboto font22 gray-text">Volver al Inicio</span>
					</div>
					<div class="space40"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>