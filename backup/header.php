<?php $wp_session= WP_Session::get_instance(); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="format-detection" content="telephone=no">
		<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?>">
		<title><?php bloginfo('name'); ?></title>
		<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo("template_directory"); ?>/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo("template_directory"); ?>/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo("template_directory"); ?>/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo("template_directory"); ?>/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo("template_directory"); ?>/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo("template_directory"); ?>/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo("template_directory"); ?>/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo("template_directory"); ?>/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo("template_directory"); ?>/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo("template_directory"); ?>/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo("template_directory"); ?>/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo("template_directory"); ?>/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo("template_directory"); ?>/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php bloginfo("template_directory"); ?>/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php bloginfo("template_directory"); ?>/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<script>(function(h){h.className = h.className.replace('no-js', 'js')})(document.documentElement)</script>
		<!-- HTML5 shim and Respond.js IE8 support for HTML5 elements and media queries. -->
		<!--[if lt IE 9]>
        <script src="<?php bloginfo("template_directory"); ?>/js/html5shiv-printshiv.min.js"></script>
        <script src="<?php bloginfo("template_directory"); ?>/js/respond.min.js"></script>
        <![endif]-->
        <?php wp_head(); ?>
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109721564-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		
		  gtag('config', 'UA-109721564-1');
		</script>
		
	</head>
	<body>
		<div class="se-pre-con"></div>
		
		<!--[if lt IE 8>
			<p class="browsehappy">
				You are using an <strong>outdated</strong> browser.
				Please <a href="http://browsehappy.com/">upgrade your browser</a>
				to improve your experience.
			</p>
	    <![endif]-->
	
		<nav>
			<div class="nav-wrapper">
				<?php if( is_page('v2') || is_page('programa') || is_page('video') || is_page('buscador') || is_page('grupo') || is_page('tematica') )  { ?>
				<a href="<?php bloginfo("url"); ?>/v2" class="brand-logo left">
				<?php } else { ?>
				<a href="<?php bloginfo("url"); ?>" class="brand-logo left">
				<?php } ?>
					<div class="hide-on-med-and-down">
						<img class="responsive-image" src="<?php bloginfo("template_directory"); ?>/img/logo_canal.png">
					</div>
					<div class="hide-on-large-only">
						<img class="responsive-image" src="<?php bloginfo("template_directory"); ?>/img/logo_white.png">
					</div>
				</a>
				<a href="#" data-activates="mobile-demo" class="button-collapse right"><i class="material-icons">menu</i></a>
				<ul class="right hide-on-med-and-down" style="margin-right: 50px;">
						<li>
							<?php if (!isset($wp_session['logged'])) { ?>
								<?php if( is_page('v2') || is_page('programa') || is_page('video') || is_page('buscador') || is_page('grupo') || is_page('tematica') )  { ?>
								<a class="avenir-next-regular font18 uppercase" href="<?php bloginfo("url"); ?>/v2/login">iniciar sesión</a>
								<?php } else { ?>
								<a class="avenir-next-regular font18 uppercase" href="<?php bloginfo("url"); ?>/login">iniciar sesión</a>
								<?php } ?>
							<?php } else { ?>
								<?php if( is_page('v2') || is_page('programa') || is_page('video') || is_page('buscador') || is_page('grupo') || is_page('tematica') )  { ?>
								<a class="avenir-next-regular font18 uppercase" href="<?php bloginfo("url"); ?>/v2/favoritos"><?php echo $wp_session['user']['nombre']; ?></a>
								<?php } else { ?>
								<a class="avenir-next-regular font18 uppercase" href="<?php bloginfo("url"); ?>/favoritos"><?php echo $wp_session['user']['nombre']; ?></a>
								<?php } ?>
							<?php } ?>
							<!-- Modal Structure -->
							<div id="modal1" class="modal">
								<div class="modal-content">
									<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/img_banner-suscribirse.png">
									<span class="closeModal">
										<a class="btnModalClose">
											<i class="fa fa-times-circle-o font30 white-text" aria-hidden="true"></i>
										</a>
									</span>
									<center>
										<span class="title uppercase white-text font30 block avenir-next-regular">
											Registrate en <span class="bold">canal once  VOD</span><br/>
										</span>
										<span class="subtitle uppercase white-text font18 block avenir-next-regular">
											y disfruta de nuestro contenido con anticipación
										</span>
										<div class="facebook-box">
											<a class="btn facebook">Conectar con Facebook</a>
										</div>
										<span class="o uppercase white-text font18 block avenir-next-regular">o</span>
										<a href="<?php bloginfo("url"); ?>/registro">
											<span class="email uppercase white-text font18 block avenir-next-regular">
												Regístrate con tu dirección de email
											</span>
										</a>
									</center>
								</div>
							</div>
						</li>
						<?php if (isset($wp_session['logged'])) { ?>
						<li>
							<div class="contenedor-btn-navbar">
								<a class="avenir-next-regular font18 uppercase" href="<?php bloginfo("url"); ?>/logout">CERRAR SESIÓN</a>
							</div>
						</li>
						<?php } ?>
						<li>
							<div class="contenedor-btn-navbar">
								<a class="avenir-next-regular font18 uppercase" target="_blank" href="http://canalonce.mx">ir a canalonce.mx</a>
							</div>
						</li>
					</ul>
					<ul class="side-nav" id="mobile-demo">
						<?php if (!isset($wp_session['logged'])) { ?>
						<li>
							<a class="avenir-next-regular font18 uppercase modal-trigger white-text centered" href="<?php bloginfo("url"); ?>/login" style="margin-top: 40px">iniciar sesión</a>
						</li>
						<li>
							<a class="avenir-next-regular font18 uppercase modal-trigger white-text centered" href="<?php bloginfo("url"); ?>/registro" style="">registro</a>
						</li>
						<?php } else { ?>
						<li>
							<a class="avenir-next-regular font18 uppercase modal-trigger white-text centered" href="<?php bloginfo("url"); ?>/favoritos" style="margin-top: 40px">FAVORITOS</a>
						</li>
						<li>
							<a class="avenir-next-regular font18 uppercase modal-trigger white-text centered" href="<?php bloginfo("url"); ?>/logout" style="margin-top: 40px">CERRAR SESIÓN</a>
						</li>
						<?php } ?>
						<li>
							<div class="contenedor-btn-navbar" style="margin: 0 auto;">
								<a class="avenir-next-regular font18 uppercase centered" target="_blank" href="http://canalonce.mx">ir a canalonce.mx</a>
							</div>
						</li>
					</ul>
				<i class="fa fa-user-circle-o right img-user font30" aria-hidden="true"></i>
			</div>
		</nav>
	
	
