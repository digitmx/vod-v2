<?php
	global $term;	
	$categoria = get_term_by('slug', $term, 'tematica');
	
	//Consulta de Posts de la Categoría
	$args = array(
		'post_type' => 'programa',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'orderby' => 'title',
		'tax_query' => array(
			array(
				'taxonomy' => 'tematica',
				'field'    => 'term_id',
				'terms'    => $categoria->term_id ,
			),
		),
	); 
	$query = new WP_Query( $args ); 
?>

<?php get_header(); ?>

		<?php $banners = get_field("banners", "option"); $contador_banners = 0; $programs_show = array(); ?>
		<?php include(locate_template('includes/slider-home.php')); ?>
		
		<?php get_template_part("includes/search","menu"); ?>
		
		<!--Series Categoria-->
		<div class="container-fluid categoria">
			<div class="row no-margin-row">
				<div class="col s12 m12 l12 padding-favoritos-series">
					<div class="space40"></div>
					<div class="contenedor-titulo-categorias">
						<span class="opns-bold-italic font36 white-text uppercase"><?php echo $categoria->name; ?></span>
						<div class="space20"></div>
					</div>
					<div class="space40"></div>
					<div class="col s12 m12 l12 contenedor-favoritos-series">
						<div class="col s12 m12 l12" style="padding: 0;">
							<?php foreach( $query->posts as $programa ) { $portada = get_field( 'logo', $programa->ID );  ?>
							<div class="col s4 m4 l3" style="padding-left: 0;">
								<div class="contenedor-img-favoritos">
									<a href="<?php echo get_permalink($programa->ID); ?>">
										<img class="responsive-img-full-w-h" src="<?php echo $portada; ?>">
									</a>
								</div>
								<div class="space20"></div>
							</div>
							<?php } ?>
						</div>
						<div class="col s12 m12 l12 no-padding">
						<div class="space40"></div>
						<div class="col s6 m4 l3" style="float: none; margin: 0 auto;">
							<a href="<?php bloginfo("url"); ?>">
								<div class="btnGreen centered">
									<span class="roboto font22 gray-text">Volver al Inicio</span>
								</div>
							</a>
							<div class="space40"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>