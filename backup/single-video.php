<?php global $post; ?>

<?php get_header(); ?>

		<?php $programa = get_field("programa", $post->ID); ?>
		<div class="container-fluid">
			<div class="row no-margin-row">
				<div class="col s12 no-padding">
					<div class="video-container">
						<div id="video-wrapper">
							<!--<div class="video-caption">
								<div class="controls">
									<div class="row box">
										<div class="col s2 m2 offset-m1 l2 offset-l1">
											<div class="right">
												<img class="responsive-img" src="<?php the_field("logo", $programa->ID); ?>" />
											</div>
										</div>
										<div class="col s10 m8 l8">
											<div class="space20 hide-on-med-and-down"></div>
											<span class="opns-bold font30 white-text block"><?php echo $programa->post_title; ?></span>
											<div class="space20 hide-on-med-and-down"></div>
											<span class="opns-regular font30 white-text block"><?php echo $post->post_title; ?></span>
											<div class="space20"></div>
											<div class="wrap">
												<input type="range" class="range" min="0" max="100" value="0">
											</div>
											<div class="space20"></div>
											<div class="left">
												<a class="btn-flat btnPlay">
													<i class="fa fa-play white-text" aria-hidden="true"></i>
												</a>
												<a class="btn-flat btnPause" style="display: none;">
													<i class="fa fa-pause white-text" aria-hidden="true"></i>
												</a>
												<a class="btn-flat btnVolume">
													<i class="fa fa-volume-up white-text" aria-hidden="true"></i>
												</a>
												<a class="btn-flat btnMute" style="display: none;">
													<i class="fa fa-volume-off white-text" aria-hidden="true"></i>
												</a>
											</div>
											<div class="right">
												<span class="opns-regular font20 white-text">
													<span class="current_time">0:00</span> /
													<span class="total_time">00:00</span>
												</span>
												<a class="btn-flat" onclick="viewPage();">
													<i class="fa fa-arrows-alt white-text" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<input type="hidden" id="video_id" name="video_id" value="<?php echo $post->ID; ?>">
							<div class="js-player" data-type="youtube" data-video-id="<?php the_field("youtube_id", $post->ID); ?>"></div>
							<div class="closeVideo">
								<a class="btn-flat btnClose">
									<i class="fa fa-times-circle-o font30 magenta-text" aria-hidden="true"></i>
								</a>
							</div>
						</div>
						
						<!--<script>
							
							//This code loads the IFrame Player API code asynchronously.
							var tag = document.createElement('script');
						
							tag.src = "https://www.youtube.com/iframe_api?key=AIzaSyCuMMmmjyYZHyhITI7-ovGSg44A2-MEHSk";
							var firstScriptTag = document.getElementsByTagName('script')[0];
							firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
						
							//This function creates an <iframe> (and YouTube player)
							//after the API code downloads.
							var player;
							function onYouTubeIframeAPIReady() {
						    	player = new YT.Player('video', {
									height: '720',
									width: '100%',
									videoId: '<?php the_field("youtube_id", $post->ID); ?>',
									playerVars: {
										start: 1,
										autoplay: 0,
										disablekb: 1,
										controls: 0,
										modestbranding: 1,
										showinfo: 0,
										enablejsapi: 1,
										origin: '<?php echo get_permalink($post->ID); ?>', 
										iv_load_policy: 3,
										cc_load_policy: 0,
										rel: 0
							   		},
							   		events: {
							   			onReady: initialize,
							   			onStateChange: onPlayerStateChange
						      		}
						    	});
						  	}
						  	
						  	function initialize(){

							    // Update the controls on load
							    updateTimerDisplay();
							    updateProgressBar();
							
							    // Clear any old interval.
							    //clearInterval(time_update_interval);
							
							    // Start interval to update elapsed time display and
							    // the elapsed part of the progress bar every second.
							    time_update_interval = setInterval(function () {
							        updateTimerDisplay();
							        updateProgressBar();
							    }, 1000)
							
							}
							
							// This function is called by initialize()
							function updateTimerDisplay()
							{
							    // Update current time text display.
							    $('.current_time').text(formatTime( player.getCurrentTime() ));
							    $('.total_time').text(formatTime( player.getDuration() ));
							}
							
							function formatTime(time)
							{
							    time = Math.round(time);
							
							    var minutes = Math.floor(time / 60),
							    seconds = time - minutes * 60;
							
							    seconds = seconds < 10 ? '0' + seconds : seconds;
							
							    return minutes + ":" + seconds;
							}
							
							// when video ends
					        function onPlayerStateChange(event) {        
					            if(event.data === 0) {          
					                $('.btnPause').hide();
									$('.btnPlay').show();
									$('.controls .box').show();
					            }
					        }
							
							$('.range').on('mouseup touchend', function (e) {

								// Calculate the new time for the video.
								// new time in seconds = total duration in seconds * ( value of range input / 100 )
								var newTime = player.getDuration() * (e.target.value / 100);

								// Skip video to new time.
								player.seekTo(newTime);
								
								switch(player.getPlayerState()) {
									case -1: // No status
										console.log('-1');
										$('.btnPlay').hide();
										$('.btnPause').show();
										$('.controls .box').delay(5000).fadeOut();
									break;
									case 0: // Terminado
										console.log('0');
										$('.btnPlay').hide();
										$('.btnPause').show();
										$('.controls .box').delay(5000).fadeOut();
									break;
									case 1: // Reproduciendo
										console.log('1');
										$('.btnPlay').hide();
										$('.btnPause').show();
										$('.controls .box').delay(5000).fadeOut();
									break;
									case 2: // Pausa
										console.log('2');
										$('.btnPause').hide();
										$('.btnPlay').show();
										$('.controls .box').show();
									break;
									case 5: // En Fila
										console.log('5');
										$('.btnPlay').hide();
										$('.btnPause').show();
										$('.controls .box').delay(5000).fadeOut();
									break;
								}

							});

							// This function is called by initialize()
							function updateProgressBar(){
								// Update the value of our progress bar accordingly.
								$('.range').val((player.getCurrentTime() / player.getDuration()) * 100);
							}
							
							//Play Button
							$('.btnPlay').on('click', function () {
								player.playVideo();
								$(this).hide();
								$('.btnPause').show();
								$('.controls .box').delay(5000).fadeOut();
							});
							
							//Pause Button
							$('.btnPause').on('click', function () {
								player.pauseVideo();
								$(this).hide();
								$('.btnPlay').show();
								$('.controls .box').show();
							});
							
							//Mute Button
							$('.btnVolume').on('click', function() {
							    
							    if(player.isMuted()){
							        player.unMute();
							    }
							    else{
							        player.mute();
							    }
							    
							    $(this).hide();
								$('.btnMute').show();
								$('.controls .box').delay(5000).fadeOut();
							});
							
							//Mute Button
							$('.btnMute').on('click', function() {
							    
							    if(player.isMuted()){
							        player.unMute();
							    }
							    else{
							        player.mute();
							    }
							    
							    $(this).hide();
								$('.btnVolume').show();
								$('.controls .box').delay(5000).fadeOut();
							});
						
							//Fullscreen
							function viewPage() {
								var el = document.getElementById('video');
								toggleFullscreen(el);
								$('.controls .box').delay(5000).fadeOut();
							};
						
							function toggleFullscreen (el) {
								if(document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement){
									if(document.exitFullscreen){
										document.exitFullscreen();
									}	else if(document.mozCancelFullScreen){
										document.mozCancelFullScreen();
									}	else if(document.webkitExitFullscreen){
										document.webkitExitFullscreen();
									}	else if(document.msExitFullscreen){
										document.msExitFullscreen();
									}
								}else{
									if(document.documentElement.requestFullscreen){
										el.requestFullscreen();
									}else if(document.documentElement.mozRequestFullScreen){
										el.mozRequestFullScreen();
									}else if(document.documentElement.webkitRequestFullscreen){
										el.webkitRequestFullscreen();
									}else if(document.documentElement.msRequestFullscreen){
										el.msRequestFullscreen();
									}
								}
							};
							
							$('.video-caption').on('click', function(e) {
								e.preventDefault();
								
								switch(player.getPlayerState()) {
									case 0: // Terminado
										player.playVideo();
										$('.btnPlay').hide();
										$('.btnPause').show();
										$('.controls .box').delay(5000).fadeOut();
									break;
									case 5: // En Fila
										player.playVideo();
										$('.btnPlay').hide();
										$('.btnPause').show();
										$('.controls .box').show();
									break;
								}
								
								return false;
							});
							
							$('.video-caption .controls').hover( function(e) {
							    $('.controls .box').show();
						    }, // over
						    function(e){ 
							    switch(player.getPlayerState()) {
									case 2: // Terminado
										$('.controls .box').show();
									break;
									default: // En Fila
										$('.controls .box').delay(5000).fadeOut();
									break;
								}
						    });  // out
						
						</script>-->
      				</div>
				</div>
			</div>
		</div>
		
		<?php get_template_part("includes/search","menu"); ?>
		
		<?php
			//Procesamos las Variables del programa
			$itunes = get_field("itunes", $programa->ID);
			$plataforma = get_field("plataforma", $programa->ID);
			$contenido = get_field("contenido",$programa->ID );
			$logo = get_field("logo",$programa->ID);
			$portada = get_field("portada",$programa->ID);
			$info = get_field("info",$programa->ID);
			$excerpt = $programa->post_excerpt;
			if (!$excerpt) { $excerpt = $programa->post_content; }
			$contenido_programa_string = (isset($_GET['content'])) ? (string)trim($_GET['content']) : '';
			$siguiente = '';	
		?>
		<div class="container-fluid episodios-series">
			<div class="row no-margin-row">
				<div class="col s12 m12 l4 hide-on-med-and-down">
					<div class="cont-img-serie">
						<img class="responsive-img" src="<?php echo $logo; ?>">
						<div class="space40"></div>
					</div>
				</div>
				<div class="col s12 m12 l8">
					<div class="space20 hide-on-med-and-down"></div>
						<div class="col s12 m12 l12">
							<div class="col s12 hide-on-large-only" style="padding: 0 2px;">
								<p class="roboto font18 white-text mobil-text-justify"><?php echo $excerpt; ?></p>
							</div>
						</div>
						<div class="col s12 m6 l6">
							<div class="col s6 m6 l5 hide-on-large-only" style="padding: 0 2px;">
								<a href="<?php echo get_permalink($post->ID); ?>">
									<div class="btnGreen centered">
										<img class="" style="width: 32px; height: 32px;" src="<?php bloginfo("template_directory"); ?>/img/img_player.png">
										<span class="roboto font22 gray-text">Reproducir</span>
									</div>
								</a>
							</div>
							<div class="col s6 m6 l8" style="padding: 0 2px;">
								<div class="btnGreen centered">
									<a class="dropdown-button btn-temporadas-slider btn font22 roboto no-padding gray-text" href="#" data-activates="contenido_<?php echo $post->ID; ?>">Contenido<i class="small material-icons">arrow_drop_down</i></a>
									<ul id="contenido_<?php echo $post->ID; ?>" class="dropdown-content black">
										<?php foreach ($contenido as $item) { ?>
										<li><a class="white-text" href="<?php echo get_permalink($post->ID); ?>?content=<?php echo sanitize_title($item['nombre']); ?>"><?php echo $item['nombre']; ?></a></li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="col s12 m6 l6">
							<div class="space10 hide-on-med-and-up"></div>
							<div class="col s6 m6 l5 hide-on-large-only">
								<a href="#">
									<div class="contenedor-btn-favorito centered">
										<i class="material-icons white-text btn-fav-img-serie">add_circle_outline</i>
										<span class="roboto font22 white-text uppercase">FAVORITOS</span>
									</div>
								</a>
							</div>
							<div class="col s6 m6 l8 float-right-serie" style="padding: 0 2px;">
								<a href="<?php bloginfo("url"); ?>">
									<div class="btnGreen centered">
										<span class="roboto font22 gray-text">Volver al Inicio</span>
									</div>
								</a>
							</div>
						</div>
						<?php if ($contenido) { ?>
							<?php
							
								$contenido_programa = array();
								$contador_contenido_programa = 0;
								foreach ($contenido as $item)
								{
									$contador_contenido_programa++;
									if (sanitize_title($item['nombre']) == $contenido_programa_string)
									{
										$contenido_programa = $item;
										break;
									}
									
									if (count($contenido) == $contador_contenido_programa)
									{
										$contenido_programa = $item;
										break;
									}
								}
							?>
							<div class="col s12 m12 l12">
								<div class="space20"></div>
								<span class="roboto bold white-text font35 uppercase"><?php echo $contenido_programa['nombre']; ?></span>
								<div class="space10"></div>
							</div>
							<div class="col s12 m10 offset-m1 l12" style="position: relative; padding-bottom: 45px;">
							<a href="#">
								<div class="btn-arriba">
									<i class="material-icons white-text" style="font-size: 3rem;">expand_less</i>
								</div>
							</a>
							<?php foreach ($contenido_programa['videos'] as $video) { ?>
								<?php
									$excerpt_video = $video->post_excerpt;
									if (!$excerpt_video) { $excerpt_video = $video->post_content; }
								?>
								<a href="<?php echo get_permalink($video->ID); ?>">
									<div class="space40"></div>
									<div class="row">
										<div class="col s6 m4 l3">
											<div class="contenedor-reproducir-episodio-serie">
												<img class="btn-play-serie" src="<?php bloginfo("template_directory"); ?>/img/img_episodios_cenado_btn_play.png">
												<img class="responsive-img-full-w-h" src="http://img.youtube.com/vi/<?php the_field("youtube_id", $video->ID); ?>/mqdefault.jpg">
											</div>
										</div>
										<div class="col s12 m8 l9">
											<span class="titulo-episodio roboto bold font23 white-text block"><?php echo $video->post_title; ?></span>
											<span class="descripcion-episodio roboto font16 white-text"><?php echo $excerpt_video; ?></span>
											<div class="space40"></div>
										</div>
									</div>
								</a>
							<?php } ?>
						</div>
						<?php } ?>
				</div>
			</div>
		</div>

		<?php
			//CONSULTAMOS ALEATORIAMENTE 8 ELEMENTOS
			$args = array(
				'post_type' => 'programa',
				'post__not_in' => array($programa->ID),
				'order' => 'ASC',
				'orderby' => 'rand',
				'posts_per_page' => 8
			); 
			$randoms = new WP_Query( $args ); 
		?>
		<div class="container-fluid asphalt">
			<div class="row no-margin-row">
				<div class="contenedor-titulo-carrusel-interes">
					<span class="opns-bold-italic font22 green-text uppercase">
						también
					</span>
					<br>
					<span class="opns-bold-italic font38 white-text uppercase">
						te puede interesar
					</span>
					<div class="space20"></div>
				</div>
			</div>
			<div class="row carrusel-edit no-margin-row">
				<?php foreach ($randoms->posts as $item) { ?>
				<div class="col s6 m6 l3 no-padding">
					<div class="contenedor-img-carrusel">
						<a class="show" rel="<?php echo $item->ID; ?>" container=".info-serie-footer" loader=".loader-footer">
							<div class="hover-ver-mas">
								<i class="material-icons white-text">add_circle_outline</i>
								<p class="avenir-next-regular font20 white-text">VER MÁS</p>
							</div>
							<img class="responsive-img" src="<?php the_field("logo",$item->ID); ?>">
						</a>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="row loader-footer centered" style="display: none;">
				<div class="space40"></div>
				<div class="preloader-wrapper big active">
					<div class="spinner-layer spinner-white-only">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
				</div>
				<div class="space40"></div>
			</div>
			<div class="row info-serie-footer"></div>
		</div>

<?php get_footer(); ?>