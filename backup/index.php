<?php get_header(); ?>

		<?php $wp_session= WP_Session::get_instance(); ?>

		<?php $banners = get_field("banners", "option"); $contador_banners = 0; $programs_show = array(); ?>
		<?php include(locate_template('includes/slider-home.php')); ?>
		
		<?php get_template_part("includes/search","menu"); ?>
		
		<?php $iduser = (isset($wp_session['user']['id'])) ? (string)trim($wp_session['user']['id']) : '0'; ?>
		
		<?php if ($iduser) { ?>
			<?php
				//CONSULTAMOS ALEATORIAMENTE 8 ELEMENTOS
				$args = array(
					'post_type' => 'log',
					'order' => 'DESC',
					'orderby' => 'rand',
					'order' => 'DESC',
					'orderby' => 'meta_value_num',
					'meta_key' => 'date',
					'meta_query' => array(
						'relation' => 'AND',
						array(
							'key' => 'usuario',
							'value' => $iduser,
							'compare' => '='
						)
					)
				); 
				$videos = new WP_Query( $args ); 
				
				//Consulta de Posts de la Categoría
				$args = array(
					'post_type' => 'favorito',
					'posts_per_page' => -1,
					'order' => 'ASC',
					'orderby' => 'title',
					'meta_query' => array(
						array(
							'key' => 'usuario',
							'value' => $iduser,
							'compare' => '='
						)
					)
				); 
				$favoritos = new WP_Query( $args ); 
			?>
			<?php if (count($videos->posts) > 0) { ?>
				<div class="container-fluid asphalt">
					<div class="row no-margin-row">
						<div class="contenedor-titulo-carrusel-interes">
							<span class="opns-bold-italic font22 green-text uppercase">
								Continuar
							</span>
							<br>
							<span class="opns-bold-italic font38 white-text uppercase">
								viendo
							</span>
							<div class="space20"></div>
						</div>
					</div>
					<div class="row my-videos no-margin-row">
						<?php foreach ($videos->posts as $item) { $video_id = get_field("video", $item->ID); $video = get_post($video_id); $programa_id = get_field("programa", $video_id); $programa = get_post($programa_id); ?>
						<div>
							<a href="<?php echo get_permalink($video->ID); ?>">
								<div class="contenedor-reproducir-episodio-serie asphalt">
									<img class="btn-play-serie" src="<?php bloginfo("template_directory"); ?>/img/img_episodios_cenado_btn_play.png">
									<img class="responsive-img-full-w-h" src="http://img.youtube.com/vi/<?php the_field("youtube_id", $video->ID); ?>/mqdefault.jpg">
									<div class="space20"></div>
									<span class="opns-bold white-text block" style="margin-left: 10px;"><?php echo $programa->post_title; ?></span>
									<span class="opns-regular white-text block" style="margin-left: 10px;"><?php echo $video->post_title; ?></span>
									<br />
								</div>
							</a>
						</div>
						<?php } ?>
						<div class="space40">
					</div>
				</div>
			<?php } ?>
			<?php if (count($favoritos->posts) > 0) { ?>
				<div class="container-fluid asphalt">
					<div class="row no-margin-row">
						<div class="contenedor-titulo-carrusel-interes">
							<span class="opns-bold-italic font22 green-text uppercase">
								Mis
							</span>
							<br>
							<span class="opns-bold-italic font38 white-text uppercase">
								Favoritos
							</span>
							<div class="space20"></div>
						</div>
					</div>
					<div class="row carrusel-edit no-margin-row">
						<?php foreach ($favoritos->posts as $item) { $programa = get_post(get_field("programa",$item->ID)); $programs_show[] = $programa->ID; ?>
						<div class="col s6 m6 l3 no-padding">
							<div class="contenedor-img-carrusel">
								<a class="show" rel="<?php echo $programa->ID; ?>" container=".info-serie-favoritos" loader=".loader-favoritos">
									<div class="hover-ver-mas">
										<i class="material-icons white-text">add_circle_outline</i>
										<p class="avenir-next-regular font20 white-text">VER MÁS</p>
									</div>
									<img class="responsive-img-full-w-h" src="<?php the_field("logo",$programa->ID); ?>">
								</a>
							</div>
						</div>
						<?php } ?>
					</div>
					<div class="row loader-favoritos centered" style="display: none;">
						<div class="space40"></div>
						<div class="preloader-wrapper big active">
							<div class="spinner-layer spinner-white-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
						<div class="space40"></div>
					</div>
					<div class="row info-serie-favoritos"></div>
				</div>
			<?php } ?>
		<?php } ?>
		
		<!--Carrusel-->
		<?php $featured = get_field('featured', 'option'); ?>
		<div class="container-fluid carrusel asphalt">
			<div class="row no-margin-row asphalt">
					<div class="contenedor-titulo-carrusel-interes">
						<span class="opns-bold-italic font22 green-text uppercase">
							Programas
						</span>
						<br>
						<span class="opns-bold-italic font38 white-text uppercase">
							Destacados
						</span>
						<div class="space20"></div>
					</div>
				</div>
			<div class="row carrusel-edit no-margin-row asphalt">
				<?php foreach ($featured as $item) { $obj = $item['program'][0]; $programs_show[] = $obj->ID; ?>
				<div class="col s6 m6 l3 no-padding">
					<div class="contenedor-img-carrusel">
						<a class="show" rel="<?php echo $obj->ID; ?>" container=".info-serie-carrousel" loader=".loader">
							<div class="hover-ver-mas">
								<i class="material-icons white-text">add_circle_outline</i>
								<p class="avenir-next-regular font20 white-text">VER MÁS</p>
							</div>
							<img class="responsive-img-full-w-h" src="<?php the_field("logo",$obj->ID); ?>">
						</a>
						<?php if ($item['text']) { ?>
						<div class="info-text-carrusel centered">
							<span class="opns-bold white-text uppercase"><?php echo $item['text']; ?></span>
						</div>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="row loader centered" style="display: none;">
				<div class="space40"></div>
				<div class="preloader-wrapper big active">
					<div class="spinner-layer spinner-white-only">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
				</div>
				<div class="space40"></div>
			</div>
			<div class="row info-serie-carrousel"></div>
		</div>
		
		<?php
			//CONSULTAMOS ALEATORIAMENTE 8 ELEMENTOS
			$args = array(
				'post_type' => 'programa',
				'post__not_in' => $programs_show,
				'order' => 'ASC',
				'orderby' => 'rand',
				'posts_per_page' => 8
			); 
			$randoms = new WP_Query( $args ); 
		?>
		<div class="container-fluid asphalt">
			<div class="row no-margin-row">
				<div class="contenedor-titulo-carrusel-interes">
					<span class="opns-bold-italic font22 green-text uppercase">
						también
					</span>
					<br>
					<span class="opns-bold-italic font38 white-text uppercase">
						te puede interesar
					</span>
					<div class="space20"></div>
				</div>
			</div>
			<div class="row carrusel-edit no-margin-row">
				<?php foreach ($randoms->posts as $item) { ?>
				<div class="col s6 m6 l3 no-padding">
					<div class="contenedor-img-carrusel">
						<a class="show" rel="<?php echo $item->ID; ?>" container=".info-serie-footer" loader=".loader-footer">
							<div class="hover-ver-mas">
								<i class="material-icons white-text">add_circle_outline</i>
								<p class="avenir-next-regular font20 white-text">VER MÁS</p>
							</div>
							<img class="responsive-img-full-w-h" src="<?php the_field("logo",$item->ID); ?>">
						</a>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="row loader-footer centered" style="display: none;">
				<div class="space40"></div>
				<div class="preloader-wrapper big active">
					<div class="spinner-layer spinner-white-only">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
				</div>
				<div class="space40"></div>
			</div>
			<div class="row info-serie-footer"></div>
		</div>
		
		<div class="container asphalt">
			<div class="row no-margin-row">
				<div class="space20"></div>
				<div class="row">
					<div class="col s12 m6 offset-m3 l4 offset-l4">
						<div class="space20"></div>
						<a href="#modal_aviso" class="modal-trigger">
							<div class="btnGreen centered">
								<span class="roboto font22 gray-text">AVISO</span>
							</div>
						</a>
					</div>
				</div>
			</div>
			<!-- Modal Structure -->
			<div id="modal_aviso" class="modal">
				<div class="modal-content black">
					<span class="black opns-regular white-text block text-justify"><?php the_field("warning","option"); ?></span>
					<div class="row">
						<div class="col s12 m6 offset-m3 l4 offset-l4">
							<div class="space20"></div>
							<a href="#!" class="modal-action modal-close">
								<div class="btnGreen centered">
									<span class="roboto font22 gray-text">Cerrar</span>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>