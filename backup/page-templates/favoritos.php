<?php /* Template Name: Favoritos */ ?>

<?php $wp_session= WP_Session::get_instance(); ?>

<?php
	//Leemos el ID del Usuario
	$iduser = $wp_session['user']['id'];
	
	//Consulta de Posts de la Categoría
	$args = array(
		'post_type' => 'favorito',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'orderby' => 'title',
		'meta_query' => array(
			array(
				'key' => 'usuario',
				'value' => $iduser,
				'compare' => '='
			)
		)
	); 
	$query = new WP_Query( $args ); 
?>

<?php get_header(); ?>

		<div class="container-fluid favoritos">
			<div class="row no-margin-row">
				<div class="col s12 m12 l12 padding-favoritos-series">
					<div class="space40"></div>
					<div class="contenedor-titulo-favoritos">
						<span class="opns-bold-italic font22 green-text uppercase" style="margin-left: 8px;">
							mis
						</span>
						<br>
						<span class="opns-bold-italic font38 white-text uppercase">
							favoritos
						</span>
						<div class="space20"></div>
					</div>
					<div class="col s12 m12 l12 contenedor-favoritos-series">
						<div class="col s12 m12 l12" style="padding: 0;">
							<?php if ($wp_session['logged']) { ?>
								<?php if (count($query->posts) > 0) { ?>
									<?php foreach( $query->posts as $favorito ) { $programa = get_post(get_field("programa",$favorito->ID)); $portada = get_field( 'logo', $programa->ID );  ?>
									<div class="col s4 m4 l3" style="padding-left: 0;">
										<div class="contenedor-img-favoritos">
											<a href="<?php echo get_permalink($programa->ID); ?>">
												<img class="responsive-img-full-w-h" src="<?php echo $portada; ?>">
											</a>
										</div>
										<div class="space20"></div>
									</div>
									<?php } ?>
								<?php } else { ?>
								<span class="opns-bold-italic font20 white-text uppercase inp-etiqueta">No existen favoritos en tu cuenta.</span>
								<?php } ?>
							<?php } else { ?>
							<span class="opns-bold-italic font20 white-text uppercase inp-etiqueta">Debes de iniciar sesión o crear una cuenta para agregar favoritos a tu cuenta.</span>
							<?php } ?>
						</div>
						<div class="col s12 m12 l12 no-padding">
						<div class="space40"></div>
						<div class="col s6 m4 l3" style="float: none; margin: 0 auto;">
							<a href="<?php bloginfo("url"); ?>">
								<div class="btnGreen centered">
									<span class="roboto font22 gray-text">Volver al Inicio</span>
								</div>
							</a>
							<div class="space40"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

		<?php
			//CONSULTAMOS ALEATORIAMENTE 8 ELEMENTOS
			$args = array(
				'post_type' => 'programa',
				//'post__not_in' => array($post->ID),
				'order' => 'ASC',
				'orderby' => 'rand',
				'posts_per_page' => 8
			); 
			$randoms = new WP_Query( $args ); 
		?>
		<div class="container-fluid asphalt">
			<div class="row no-margin-row">
				<div class="contenedor-titulo-carrusel-interes">
					<span class="opns-bold-italic font22 green-text uppercase">
						también
					</span>
					<br>
					<span class="opns-bold-italic font38 white-text uppercase">
						te puede interesar
					</span>
					<div class="space20"></div>
				</div>
			</div>
			<div class="row carrusel-edit no-margin-row">
				<?php foreach ($randoms->posts as $item) { ?>
				<div class="col s6 m6 l3 no-padding">
					<div class="contenedor-img-carrusel">
						<a class="show" rel="<?php echo $item->ID; ?>">
							<div class="hover-ver-mas">
								<i class="material-icons white-text">add_circle_outline</i>
								<p class="avenir-next-regular font20 white-text">VER MÁS</p>
							</div>
							<img class="responsive-img-full-w-h" src="<?php the_field("logo",$item->ID); ?>">
						</a>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="row info-serie-footer"></div>
		</div>

<?php get_footer(); ?>