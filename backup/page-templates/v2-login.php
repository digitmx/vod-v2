<?php $wp_session= WP_Session::get_instance(); ?>

<?php if (!$wp_session['logged']) { ?> 

<?php get_header(); ?>

<div class="container-fluid registro-usuario">
	<div class="row no-margin-row">
		<div class="col s12 m12 l10 offset-l1">
			<form id="formRegistro" name="formRegistro" accept-charset="utf-8" method="post" action="<?php bloginfo("url"); ?>">
				<div class="space40"></div>
				<div class="col s12 m12 l12">
					<div class="contenedor-titulo-registro">
						<span class="opns-bold-italic font22 green-text uppercase" style="margin-left: 8px;">
							Inicio
						</span>
						<br>
						<span class="opns-bold-italic font38 white-text uppercase">
							de Sesión
						</span>
						<div class="space40"></div>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m6 l6 no-padding">
						<div class="input-field col s12">
							<input autocomplete="off" placeholder="" id="inputEmail" name="inputEmail" type="email" class="validate inp-registro">
							<label class="opns-bold-italic font20 white-text uppercase inp-etiqueta" for="inputEmail">correo electrónico</label>
						</div>
					</div>
					<div class="col s12 m6 l6 no-padding">
						<div class="input-field col s12">
							<input autocomplete="off" placeholder="" id="inputPassword" name="inputPassword" type="password" class="validate inp-registro">
							<label class="opns-bold-italic font20 white-text uppercase inp-etiqueta" for="inputPassword">contraseña</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 opns-regular uppercase editar-checkbox">
						<div class="col s12 m12 l12 no-padding">
							<div class="space40"></div>
							<div class="col s5 m3 l3" style="float: none; margin: 0 auto;">
								<div class="btnGreen centered doLogin">
									<span class="roboto font22 gray-text uppercase">entrar</span>
								</div>
							</div>
							<div class="col s12 centered">
								<div class="space20"></div>
								<a class="btn-flat white-text" href="<?php bloginfo("url"); ?>/recuperar">
									¿Olvidaste tu contraseña?
								</a>
								<div class="space40"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 no-padding centered">
						<span class="opns-bold-italic font22 white-text uppercase">
							Si aún no tienes un usuario
						</span>
					</div>
					<div class="col s12 m12 l12 no-padding">
						<div class="space40"></div>
						<div class="col s6 m4 l3" style="float: none; margin: 0 auto;">
							<a class="registro" href="#">
								<div class="btnGreen centered">
									<span class="roboto font22 gray-text">REGISTRATE</span>
								</div>
							</a>
							<div class="space40"></div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<?php get_footer(); ?>

<?php } else { wp_redirect( home_url() ); exit; } ?>