		
		<!--Slider-->
		<div class="container-fluid">
			<div class="row" style="margin-bottom: 0;">
				<div class="col s12 m12 l12 no-padding">
					 <div class="slider">
						<ul class="slides" style="position: relative;">
							<?php $portadas['1'] = get_bloginfo("template_directory") . '/img/banner_islas-de-mexico_v2_logo3.jpg'; ?>
							<?php $portadas['2'] = get_bloginfo("template_directory") . '/img/banner_noctambulos_v2_logo3.jpg'; ?>
							<?php $portadas['3'] = get_bloginfo("template_directory") . '/img/banner_noches-boleros-y-son_v2_logo_3.jpg'; ?>
							<?php foreach ($banners as $banner) { $contador_banners++; ?>
							<?php $programa = $banner['program'][0]; $programs_show[] = $programa->ID; ?>
							<?php $contenido = get_field("contenido",$programa->ID ); ?>
							<?php
								//Obtenemos la URL del Video Siguiente
								$siguiente = '';
								if ($contenido) 
								{
									foreach ($contenido as $item)
									{
										foreach ($item['videos'] as $video)
										{
											$siguiente = get_permalink($video->ID);
											break;
										}
									}
								}	
							?>
							<li>
								<?php if( is_page('v2') || is_page('programa') || is_page('video') || is_page('buscador') || is_page('grupo') || is_page('tematica') )  { ?>
								<img class="responsive-img-full-w-h show-on-large hide-on-med-and-down" src="<?php echo $portadas[$contador_banners]; ?>">
								<?php } else { ?>
								<img class="responsive-img-full-w-h show-on-large hide-on-med-and-down" src="<?php echo $banner['desktop']; ?>">
								<?php } ?>
								<img class="responsive-img-full-w-h hide-on-large-only" src="<?php echo $banner['mobile']; ?>">
								<div class="caption">
									<div class="row no-margin-row-mobile">
										<div class="col s12 m12 l12 centered-med-and-down">
											<span class="roboto bold font35 uppercase padding-left-temporada"><?=($banner['title']) ? $banner['title'] : '&nbsp;'; ?></span>
											<div class="space10"></div>
										</div>
										<div class="col s12 m6 l12">
											<div class="col s6 m6 l5" style="padding: 0 2px;">
												<a href="<?php echo $siguiente; ?>">
													<div class="btnGreen centered">
														<?php if( is_page('v2') || is_page('programa') || is_page('video') || is_page('buscador') || is_page('grupo') || is_page('tematica') )  { ?>
														<i class="fa fa-play-circle-o font24 white-text" aria-hidden="true"></i>
														<?php } else { ?>
														<img class="" style="width: 32px; height: 32px;" src="<?php bloginfo("template_directory"); ?>/img/img_player.png">
														<?php } ?>
														<span class="roboto font22 gray-text">Reproducir</span>
													</div>
												</a>
											</div>
											<!--
											<div class="col s6 m6 l5" style="padding: 0 2px;">
												<div class="contenedor-reprodicir-slider centered">
													<a class="dropdown-button btn-temporadas-slider btn font22 roboto no-padding gray-text" href="#" data-activates="banner_season_<?=$contador_banners;?>">Temporadas<i class="small material-icons">arrow_drop_down</i></a>
													
													<ul id="banner_season_<?=$contador_banners;?>" class="dropdown-content">
														<li><a href="#!">three</a></li>
														<li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
														<li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
													</ul>
												</div>
											</div>
											-->
										</div>
										<div class="col s12 m12 l12 hide-on-med-and-down">
											<p class="roboto font18">
												<?=($banner['description']) ? $banner['description'] : '&nbsp;'; ?>
											</p>
										</div>
										<div class="col s12 m6 l12">
											<div class="space10 hide-on-med-and-up"></div>
											<div class="col s6 m6 l4" style="padding: 0 2px;">
												<?php if( is_page('v2') || is_page('programa') || is_page('video') || is_page('buscador') || is_page('grupo') || is_page('tematica') )  { ?>
												<a href="<?php echo get_bloginfo("url").'/v2/programa/?id='.$programa->ID; ?>">
												<?php } else { ?>
												<a href="<?php echo get_permalink($programa->ID); ?>">
												<?php } ?>
													<div class="contenedor-btn-mas centered">
														<span class="roboto font18 white-text">VER MÁS</span>
													</div>
													<div class="space10 hide-on-med-and-up"></div>
												</a>
											</div>
											<div class="col s6 m6 l5">
												<a href="#">
													<div class="contenedor-btn-favorito centered" rel="<?php echo $programa->ID; ?>">
														<i class="material-icons white-text btn-fav-img">add_circle_outline</i>
														<span class="roboto font22 white-text uppercase btn-favoritos">FAVORITOS</span>
													</div>
													<div class="space10 hide-on-med-and-up"></div>
												</a>
											</div>
										</div>
									</div>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>