
		<!--Programacion y Buscador-->
		<?php
			/* Leemos los Grupos */
			$grupos = get_terms( array(
			    'taxonomy' => 'grupo',
			    'hide_empty' => false,
			) );
			
			/* Leemos las Tematicas */
			$tematicas = get_terms( array(
			    'taxonomy' => 'tematica',
			    'hide_empty' => false,
			) );	
		?>
		<div class="container-fluid black">
			<div class="row">
				<div class="col l5 hide-on-med-and-down">
					<div class="col s12 m12 l7 no-padding centered" style="line-height: 70px">
						<!-- Dropdown Trigger -->
						<a class="dropdown-button btn font22 roboto black no-padding" href="#" data-activates="grupos">PROGRAMAS A - Z<i class="small material-icons">arrow_drop_down</i></a>
						
						<!-- Dropdown Structure -->
						<ul id="grupos" class="dropdown-content black">
							<?php foreach ($grupos as $term) { ?>
							<li><a class="white-text" href="<?php echo get_bloginfo("url"); ?>/v2/grupo/?id=<?php echo $term->term_id; ?>"><?php echo $term->name; ?></a></li>
							<?php } ?>
						</ul>
					</div>
					<div class="col s12 m12 l5 no-padding centered" style="line-height: 70px;">
						<!-- Dropdown Trigger -->
						<a class="dropdown-button btn font22 roboto black uppercase no-padding" href="#" data-activates="tematicas">categorías<i class="small material-icons">arrow_drop_down</i></a>
						
						<!-- Dropdown Structure -->
						<ul id="tematicas" class="dropdown-content black">
							<?php foreach ($tematicas as $term) { ?>
							<li><a class="white-text" href="<?php echo get_bloginfo("url"); ?>/v2/tematica/?id=<?php echo $term->term_id; ?>"><?php echo $term->name; ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="col s10 m11 l6">
					<form>
						<div class="input-field">
							<input id="s" name="s" class="editar-busqueda" type="search" value="<?=(isset($_GET['q'])) ? (string)trim($_GET['q']) : ''; ?>" placeholder="Introduce el nombre del programa que deseas buscar">
							<label class="label-icon" for="search"><i class="material-icons"></i></label>
							<!--<i class="material-icons">close</i>-->
						</div>
					</form>
				</div>
				<div class="col s2 m1 l1">
					<i class="material-icons white-text icon-busqueda">search</i></label>
				</div>
			</div>
		</div>