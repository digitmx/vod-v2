<?php

	//Search Term
	$term = (isset($_GET['s'])) ? (string)trim($_GET['s']) : '';
	
	//Verificamos si existe el Termino
	if ($term)
	{
		//Buscamos los Programas
		$args = array(
			'post_type' => 'programa',
			's' => $term,
			'posts_per_page' => -1,
			'order' => 'ASC',
			'orderby' => 'title'
		); 
		$query = new WP_Query( $args ); 
	}
	else
	{
		$query = false;	
	}	
	
?>

<?php get_header(); ?>

		<?php get_template_part("includes/search","menu"); ?>
		
		<!--Series Categoria-->
		<div class="container-fluid categoria">
			<div class="row no-margin-row">
				<div class="col s12 m12 l12 padding-favoritos-series">
					<div class="space40"></div>
					<div class="contenedor-titulo-categorias">
						<?php if ($query->posts) { ?>
						<span class="opns-bold-italic font36 white-text">Buscar: <span class="uppercase"><?=(isset($_GET['s'])) ? (string)trim($_GET['s']) : ''; ?></span></span>
						<?php } ?>
						<div class="space20"></div>
					</div>
					<div class="space40"></div>
					<?php if ($query->posts) { ?>
					<div class="col s12 m12 l12 contenedor-favoritos-series">
						<div class="col s12 m12 l12" style="padding: 0;">
							<?php foreach( $query->posts as $programa ) { $portada = get_field( 'logo', $programa->ID );  ?>
							<div class="col s4 m4 l3" style="padding-left: 0;">
								<div class="contenedor-img-favoritos">
									<a href="<?php echo get_permalink($programa->ID); ?>">
										<img class="responsive-img-full-w-h" src="<?php echo $portada; ?>">
									</a>
								</div>
								<div class="space20"></div>
							</div>
							<?php } ?>
						</div>
						<div class="col s12 m12 l12 no-padding">
							<div class="space40"></div>
							<div class="col s6 m4 l3" style="float: none; margin: 0 auto;">
								<a href="<?php bloginfo("url"); ?>">
									<div class="btnGreen centered">
										<span class="roboto font22 gray-text">Volver al Inicio</span>
									</div>
								</a>
								<div class="space40"></div>
							</div>
						</div>
					</div>
					<?php } else { ?>
					<div class="col s12 m12 l12">
						<div class="centered">
							<span class="opns-bold-italic font36 white-text">"<?php echo $term; ?>" no encontrado.</span>
						</div>
						<div class="space40"></div>
						<div class="container">
							<div class="row no-margin-row">
								<div class="space20"></div>
								<span class="opns-bold-italic font22 green-text uppercase">
									AVISO
								</span>
							</div>
							<div class="row">
								<span class="opns-regular white-text block text-justify"><?php the_field("warning","option"); ?></span>
								<div class="space20"></div>
							</div>
						</div>
						<div class="col s6 m4 l3" style="float: none; margin: 0 auto;">
							<a href="<?php bloginfo("url"); ?>">
								<div class="btnGreen centered">
									<span class="roboto font22 gray-text">Volver al Inicio</span>
								</div>
							</a>
							<div class="space40"></div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		
<?php get_footer(); ?>