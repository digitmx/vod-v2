<?php
	
	/* SESSION */
	ob_start();
    $wp_session= WP_Session::get_instance();
    
    //Session Expiration (https://github.com/ericmann/wp-session-manager)
    add_filter( 'wp_session_expiration', function() { return 60 * 20160; } ); // Seconds

	//Remove Enqueue Scripts
	remove_action( 'wp_enqueue_scripts', 'required_load_scripts' );

	if( function_exists('acf_add_options_page') ) {

		acf_add_options_page();

	}

	// Thumbnails Support
	if ( function_exists( 'add_theme_support' ) ) {
	  add_theme_support( 'post-thumbnails' );
	}

	//CHANGE POST MENU LABELS
	function change_post_menu_label() {
	    global $menu;
	    global $submenu;
	    $menu[70][0] = 'Administradores';
	    echo '';
	}
    add_action( 'admin_menu', 'change_post_menu_label' );

	//Change Footer Text
	add_filter( 'admin_footer_text', 'my_footer_text' );
	add_filter( 'update_footer', 'my_footer_version', 11 );
	function my_footer_text() {
	    return '<i>Canal Once TV</i>';
	}
	function my_footer_version() {
	    return 'Version 1.0';
	}

	// Páginas de Configuración
	add_filter('acf/options_page/settings', 'my_options_page_settings');

	function my_options_page_settings ( $options )
	{
		$options['title'] = __('Configuración');
		$options['pages'] = array(
			__('Home'),
			__('Footer')
		);

		return $options;
	}

	/* Definición de Directorios */
	define( 'JSPATH', get_template_directory_uri() . '/js/' );
	define( 'CSSPATH', get_template_directory_uri() . '/css/' );
	define( 'THEMEPATH', get_template_directory_uri() . '/' );
	define( 'IMGPATH', get_template_directory_uri() . '/img/' );
	define( 'SITEURL', site_url('/') );

	/* Enqueue scripts and styles. */
	function scripts() {
		// Load CSS
		wp_enqueue_style( 'fontMaterialDesign', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
		wp_enqueue_style( 'plyr', CSSPATH . 'plyr.css', array(), '2.0.18' );
		
		if( is_page('v2') || is_page('programa') || is_page('video') || is_page('buscador') || is_page('grupo') || is_page('tematica') ) 
		{
			wp_enqueue_style( 'styles', CSSPATH . 'custom_app.css', array(), '1.0.17' );
		}
		else
		{
			wp_enqueue_style( 'styles', CSSPATH . 'app.css', array(), '1.0.126' );
		}
		
		// Load JS
		wp_deregister_script('jquery');
		wp_enqueue_script('plyr', JSPATH . 'plyr.js', array(), '2.0.18', false );
		wp_enqueue_script('rangetouch', JSPATH . 'rangetouch.js', array(), '1.0.0', false );
		
		if( is_page('v2') || is_page('programa') || is_page('video') || is_page('buscador') || is_page('grupo') || is_page('tematica') ) 
		{
			wp_enqueue_script('jquery', JSPATH . 'custom_app.js', array(), '1.0.14', false );
		}
		else
		{
			wp_enqueue_script('jquery', JSPATH . 'app.js', array(), '1.0.143', false );
		}
	}
	add_action( 'wp_enqueue_scripts', 'scripts' );

	//hook into the init action and call create_book_taxonomies when it fires
	add_action( 'init', 'tematica', 0 );
	
	//create a custom taxonomy name it topics for your posts
	
	function tematica() {
	
	// Add new taxonomy, make it hierarchical like categories
	//first do the translations part for GUI
	
	  $labels = array(
	    'name' => __( 'Temáticas' ),
	    'singular_name' => __( 'Temática' ),
	    'search_items' =>  __( 'Buscar Temática' ),
	    'all_items' => __( 'Todos los Temáticas' ),
	    'parent_item' => __( 'Parent Temática' ),
	    'parent_item_colon' => __( 'Parent Temática:' ),
	    'edit_item' => __( 'Editar Temática' ), 
	    'update_item' => __( 'Actualizar Temática' ),
	    'add_new_item' => __( 'Agregar Temática' ),
	    'new_item_name' => __( 'Nombre Nueva Temática' ),
	    'menu_name' => __( 'Temáticas' ),
	  ); 	
	
	// Now register the taxonomy
	
	  register_taxonomy('tematica',array('programa'), array(
	    'hierarchical' => true,
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true,
	    'show_admin_column' => true,
	    'query_var' => true,
	    'sort' => true,
	    'capability_type' => 'post',
	    'rewrite' => array( 'slug' => 'tematica' ),
	  ));
	
	}
	
	//hook into the init action and call create_book_taxonomies when it fires
	add_action( 'init', 'grupo', 0 );
	
	//create a custom taxonomy name it topics for your posts
	
	function grupo() {
	
	// Add new taxonomy, make it hierarchical like categories
	//first do the translations part for GUI
	
	  $labels = array(
	    'name' => __( 'Grupos' ),
	    'singular_name' => __( 'Grupo' ),
	    'search_items' =>  __( 'Buscar Grupo' ),
	    'all_items' => __( 'Todos los Grupos' ),
	    'parent_item' => __( 'Parent Grupo' ),
	    'parent_item_colon' => __( 'Parent Grupo:' ),
	    'edit_item' => __( 'Editar Grupo' ), 
	    'update_item' => __( 'Actualizar Grupo' ),
	    'add_new_item' => __( 'Agregar Grupo' ),
	    'new_item_name' => __( 'Nombre Grupo Nuevo' ),
	    'menu_name' => __( 'Grupos' ),
	  ); 	
	
	// Now register the taxonomy
	
	  register_taxonomy('grupo',array('programa'), array(
	    'hierarchical' => true,
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true,
	    'show_admin_column' => true,
	    'query_var' => true,
	    'sort' => true,
	    'capability_type' => 'post',
	    'rewrite' => array( 'slug' => 'grupo' ),
	  ));
	
	}
	
	//hook into the init action and call create_book_taxonomies when it fires
	add_action( 'init', 'elenco', 0 );
	
	//create a custom taxonomy name it topics for your posts
	
	function elenco() {
	
	// Add new taxonomy, make it hierarchical like categories
	//first do the translations part for GUI
	
	  $labels = array(
	    'name' => __( 'Elenco' ),
	    'singular_name' => __( 'Elenco' ),
	    'search_items' =>  __( 'Buscar Elenco' ),
	    'all_items' => __( 'Todos el Elenco' ),
	    'parent_item' => __( 'Parent Elenco' ),
	    'parent_item_colon' => __( 'Parent Elenco:' ),
	    'edit_item' => __( 'Editar Elenco' ), 
	    'update_item' => __( 'Actualizar Elenco' ),
	    'add_new_item' => __( 'Agregar Elenco' ),
	    'new_item_name' => __( 'Nombre Elenco Nuevo' ),
	    'menu_name' => __( 'Elenco' ),
	  ); 	
	
	// Now register the taxonomy
	
	  register_taxonomy('elenco',array('programa'), array(
	    'hierarchical' => true,
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true,
	    'show_admin_column' => true,
	    'query_var' => true,
	    'sort' => true,
	    'capability_type' => 'post',
	    'rewrite' => array( 'slug' => 'elenco' ),
	  ));
	
	}

	//CUSTOM POST TYPES
	add_action( 'init', 'codex_custom_init' );
	function codex_custom_init() {
		
		//Programas
		$labels = array(
		    'name' => _x('Programas', 'post type general name'),
		    'singular_name' => _x('Programa', 'post type singular name'),
		    'add_new' => _x('Agregar Nuevo', 'programa'),
		    'add_new_item' => __('Agregar Nuevo Programa'),
		    'edit_item' => __('Editar Programa'),
		    'new_item' => __('Nuevo Programa'),
		    'all_items' => __('Todos los Programas'),
		    'view_item' => __('Ver Programa'),
		    'search_items' => __('Buscar Programas'),
		    'not_found' =>  __('No encontrado'),
		    'not_found_in_trash' => __('No encontrado en Papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Programas'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-format-image',
		    'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields' )
		);
		register_post_type('programa',$args);
		
		//Videos
		$labels = array(
		    'name' => _x('Videos', 'post type general name'),
		    'singular_name' => _x('Video', 'post type singular name'),
		    'add_new' => _x('Agregar Nuevo', 'video'),
		    'add_new_item' => __('Agregar Nuevo Video'),
		    'edit_item' => __('Editar Video'),
		    'new_item' => __('Nuevo Video'),
		    'all_items' => __('Todos los Videos'),
		    'view_item' => __('Ver Video'),
		    'search_items' => __('Buscar Videos'),
		    'not_found' =>  __('No encontrado'),
		    'not_found_in_trash' => __('No encontrado en Papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Videos'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-video-alt3',
		    'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields' )
		);
		register_post_type('video',$args);
		
		//Usuarios
		$labels = array(
		    'name' => _x('Usuarios', 'post type general name'),
		    'singular_name' => _x('Usuario', 'post type singular name'),
		    'add_new' => _x('Agregar Nuevo', 'usuario'),
		    'add_new_item' => __('Agregar Nuevo Usuario'),
		    'edit_item' => __('Editar Usuario'),
		    'new_item' => __('Nuevo Usuario'),
		    'all_items' => __('Todos los Usuarios'),
		    'view_item' => __('Ver Usuario'),
		    'search_items' => __('Buscar Usuarios'),
		    'not_found' =>  __('No encontrado'),
		    'not_found_in_trash' => __('No encontrado en Papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Usuarios'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-admin-users',
		    'supports' => array( 'title', 'custom-fields' )
		);
		register_post_type('usuario',$args);
		
		//Favoritos
		$labels = array(
		    'name' => _x('Favoritos', 'post type general name'),
		    'singular_name' => _x('Favorito', 'post type singular name'),
		    'add_new' => _x('Agregar Nuevo', 'favorito'),
		    'add_new_item' => __('Agregar Nuevo Favorito'),
		    'edit_item' => __('Editar Favorito'),
		    'new_item' => __('Nuevo Favorito'),
		    'all_items' => __('Todos los Favoritos'),
		    'view_item' => __('Ver Favorito'),
		    'search_items' => __('Buscar Favoritos'),
		    'not_found' =>  __('No encontrado'),
		    'not_found_in_trash' => __('No encontrado en Papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Favoritos'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-heart',
		    'supports' => array( 'title', 'custom-fields' )
		);
		register_post_type('favorito',$args);
		
		//Logs
		$labels = array(
		    'name' => _x('Logs', 'post type general name'),
		    'singular_name' => _x('Log', 'post type singular name'),
		    'add_new' => _x('Agregar Nuevo', 'log'),
		    'add_new_item' => __('Agregar Nuevo Log'),
		    'edit_item' => __('Editar Log'),
		    'new_item' => __('Nuevo Log'),
		    'all_items' => __('Todos los Logs'),
		    'view_item' => __('Ver Log'),
		    'search_items' => __('Buscar Logs'),
		    'not_found' =>  __('No encontrado'),
		    'not_found_in_trash' => __('No encontrado en Papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Logs'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-welcome-write-blog',
		    'supports' => array( 'title', 'custom-fields' )
		);
		register_post_type('log',$args);
		
	}

	//Funcion para identar JSON
	function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo indent($json);
	}

	//SUBIR IMAGEN A CAMPO IMG DE ACF
	function my_update_attachment($f,$pid,$t='',$c='') {
	  	wp_update_attachment_metadata( $pid, $f );
	  	if( !empty( $_FILES[$f]['name'] )) { //New upload
	    	require_once( ABSPATH . 'wp-admin/includes/file.php' );
			include( ABSPATH . 'wp-admin/includes/image.php' );
			// $override['action'] = 'editpost';
			$override['test_form'] = false;
			$file = wp_handle_upload( $_FILES[$f], $override );

			if ( isset( $file['error'] )) {
				return new WP_Error( 'upload_error', $file['error'] );
	    	}

			$file_type = wp_check_filetype($_FILES[$f]['name'], array(
				'jpg|jpeg' => 'image/jpeg',
				'gif' => 'image/gif',
				'png' => 'image/png',
			));

			if ($file_type['type']) {
				$name_parts = pathinfo( $file['file'] );
				$name = $file['filename'];
				$type = $file['type'];
				$title = $t ? $t : $name;
				$content = $c;

				$attachment = array(
					'post_title' => $title,
					'post_type' => 'attachment',
					'post_content' => $content,
					'post_parent' => $pid,
					'post_mime_type' => $type,
					'guid' => $file['url'],
				);

				foreach( get_intermediate_image_sizes() as $s ) {
					$sizes[$s] = array( 'width' => '', 'height' => '', 'crop' => true );
					$sizes[$s]['width'] = get_option( "{$s}_size_w" ); // For default sizes set in options
					$sizes[$s]['height'] = get_option( "{$s}_size_h" ); // For default sizes set in options
					$sizes[$s]['crop'] = get_option( "{$s}_crop" ); // For default sizes set in options
	      		}

		  		$sizes = apply_filters( 'intermediate_image_sizes_advanced', $sizes );

		  		foreach( $sizes as $size => $size_data ) {
		  			$resized = image_make_intermediate_size( $file['file'], $size_data['width'], $size_data['height'], $size_data['crop'] );
		  			if ( $resized )
		  				$metadata['sizes'][$size] = $resized;
	      		}

		  		$attach_id = wp_insert_attachment( $attachment, $file['file'] /*, $pid - for post_thumbnails*/);

		  		if ( !is_wp_error( $attach_id )) {
		  			$attach_meta = wp_generate_attachment_metadata( $attach_id, $file['file'] );
		  			wp_update_attachment_metadata( $attach_id, $attach_meta );
	      		}

		  		return array(
		  			'pid' =>$pid,
		  			'url' =>$file['url'],
		  			'file'=>$file,
		  			'attach_id'=>$attach_id
		  		);
	    	}
	  	}
	}
	
	//Function to Replace Variables
	function replace_content($content)
	{
		$wp_session= WP_Session::get_instance();
	
		//WELCOME USER
		if ($wp_session['mail_welcome_name']) { $content = str_replace('@nombre@', $wp_session['mail_welcome_name'], $content); unset($wp_session['mail_welcome_name']); }
		if ($wp_session['mail_welcome_password']) { $content = str_replace('@password@', $wp_session['mail_welcome_password'], $content); unset($wp_session['mail_welcome_password']); }
		if ($wp_session['mail_welcome_email']) { $content = str_replace('@email@', $wp_session['mail_welcome_email'], $content); unset($wp_session['mail_welcome_email']); }
		if ($wp_session['mail_welcome_genero']) { $content = str_replace('@genero@', $wp_session['mail_welcome_genero'], $content); unset($wp_session['mail_welcome_genero']); }
		if ($wp_session['mail_welcome_fecha']) { $content = str_replace('@fecha@', $wp_session['mail_welcome_fecha'], $content); unset($wp_session['mail_welcome_fecha']); }
		
		//FORGOT PASSWORD USER
		if ($wp_session['mail_forgot_name']) { $content = str_replace('@nombre@', $wp_session['mail_forgot_name'], $content); unset($wp_session['mail_forgot_name']); }
		if ($wp_session['mail_forgot_email']) { $content = str_replace('@email@', $wp_session['mail_forgot_email'], $content); unset($wp_session['mail_forgot_email']); }
		if ($wp_session['mail_forgot_link']) { $content = str_replace('@link@', $wp_session['mail_forgot_link'], $content); unset($wp_session['mail_forgot_link']); }
		
		return $content;
	}
	add_filter('the_content','replace_content');

?>