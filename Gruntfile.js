'use strict';
module.exports = function(grunt) {

    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({

		watch: {
		    js: {
		        files: ['js/functions.js'],
		        tasks: ['concat:js','uglify:js']
    		},
    		css: {
	    		files: ['style.css'],
		        tasks: ['concat:css','cssmin:css']
    		},
    		php: {
	    		files: ['*.php','page-templates/*.php','includes/*.php'],
		        tasks: ['copy']
    		},
    		custom_js: {
		        files: ['js/custom.js'],
		        tasks: ['concat:custom_js','uglify:custom_js']
    		},
    		custom_css: {
	    		files: ['css/custom.css'],
		        tasks: ['concat:custom_css','cssmin:custom_css']
    		},
		},

        copy: {
		  	main: {
		    	files: [
					// includes files within path and its sub-directories
					{expand: true, src: ['style.css'], dest: 'dist/'},
					{expand: true, src: ['*.php'], dest: 'dist/'},
					{expand: true, src: ['fonts/roboto/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/*'], dest: 'dist/'},
					{expand: true, src: ['favicon/*'], dest: 'dist/'},
					{expand: true, src: ['screenshot.png'], dest: 'dist/'},
					{expand: true, src: ['js/html5shiv-printshiv.min.js'], dest: 'dist/'},
					{expand: true, src: ['js/respond.min.js'], dest: 'dist/'},
					{expand: true, src: ['page-templates/*'], dest: 'dist/'},
					{expand: true, src: ['includes/*'], dest: 'dist/'},
					{expand: true, src: ['css/fonts/*'], dest: 'dist/'},
					{expand: true, src: ['css/ajax-loader.gif'], dest: 'dist/'},
					{expand: true, src: ['css/foundation.css'], dest: 'dist/'},
					{expand: true, src: ['css/plyr.svg'], dest: 'dist/'},
					{expand: true, src: ['css/plyr.css'], dest: 'dist/'},
					{expand: true, src: ['js/plyr.js'], dest: 'dist/'},
					{expand: true, src: ['js/rangetouch.js'], dest: 'dist/'},
					{expand: true, src: ['stores/*'], dest: 'dist/'},
				],
		  	},
		},

        concat: {
            css: {
                src: [
                    'css/materialize.min.css','css/slick.css','css/slick-theme.css','css/normalize.css','style.css'
                ],
                dest: 'combined/combined.css'
            },
            js: {
                src: [
                    'js/jquery-3.2.1.min.js','js/materialize.min.js','js/slick.min.js','js/functions.js'
                ],
                dest: 'combined/combined.js'
            },
            custom_js: {
                src: [
                    'js/jquery-3.2.1.min.js','js/materialize.min.js','js/slick.min.js','js/custom.js'
                ],
                dest: 'combined/combined_custom.js'
            },
            custom_css: {
                src: [
                    'css/materialize.min.css','css/slick.css','css/slick-theme.css','css/normalize.css','css/custom.css'
                ],
                dest: 'combined/combined_custom.css'
            },
        },

        cssmin: {
            css: {
                src: 'combined/combined.css',
                dest: 'dist/css/app.css'
            },
            css_local: {
                src: 'combined/combined.css',
                dest: 'css/app.css'
            },
            custom_css: {
                src: 'combined/combined_custom.css',
                dest: 'dist/css/custom_app.css'
            },
        },

        uglify: {
            js: {
                files: {
                    'dist/js/app.js': ['combined/combined.js']
                }
            },
            js_local: {
                files: {
                    'js/app.js': ['combined/combined.js']
                }
            },
            custom_js: {
                files: {
                    'dist/js/custom_app.js': ['combined/combined_custom.js']
                }
            },
        },

        // image optimization
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 7,
                    progressive: true
                },
                files: [{
                    expand: true,
                    cwd: 'images/',
                    src: '**/*',
                    dest: 'dist/img/'
                },
                {
                    expand: true,
                    cwd: 'images/',
                    src: '**/*',
                    dest: 'img/'
                }]
            }
        },

        clean: ["dist/"]

    });

    // register task
    grunt.registerTask('default', ['clean', 'imagemin', 'concat', 'cssmin', 'uglify', 'copy']);

};