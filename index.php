<?php get_header(); ?>

		<?php $wp_session= WP_Session::get_instance(); ?>

		<?php $banners = get_field("banners", "option"); $contador_banners = 0; $programs_show = array(); ?>
		<?php include(locate_template('includes/slider-home.php')); ?>
		
		<?php get_template_part("includes/search","menuv2"); ?>
		
		<?php $iduser = (isset($wp_session['user']['id'])) ? (string)trim($wp_session['user']['id']) : '0'; ?>
		
		<?php if ($iduser) { ?>
			<?php
				//CONSULTAMOS ALEATORIAMENTE 8 ELEMENTOS
				$args = array(
					'post_type' => 'log',
					'order' => 'DESC',
					'orderby' => 'rand',
					'order' => 'DESC',
					'orderby' => 'meta_value_num',
					'meta_key' => 'date',
					'meta_query' => array(
						'relation' => 'AND',
						array(
							'key' => 'usuario',
							'value' => $iduser,
							'compare' => '='
						)
					)
				); 
				$videos = new WP_Query( $args ); 
				
				//Consulta de Posts de la Categoría
				$args = array(
					'post_type' => 'favorito',
					'posts_per_page' => -1,
					'order' => 'ASC',
					'orderby' => 'title',
					'meta_query' => array(
						array(
							'key' => 'usuario',
							'value' => $iduser,
							'compare' => '='
						)
					)
				); 
				$favoritos = new WP_Query( $args ); 
			?>
			<?php if (count($videos->posts) > 0) { ?>
				<div class="container-fluid">
					<div class="row no-margin-row">
						<div class="contenedor-titulo-carrusel-interes">
							<span class="opns-bold-italic font22 green-text uppercase">
								Continuar viendo
							</span>
							<div class="space20"></div>
						</div>
					</div>
					<div class="row my-videos no-margin-row">
						<?php foreach ($videos->posts as $item) { $video_id = get_field("video", $item->ID); $video = get_post($video_id); $programa_id = get_field("programa", $video_id); $programa = get_post($programa_id); ?>
						<div style="margin: 0px 4px;">
							<a href="<?php echo get_permalink($video->ID); ?>">
								<div class="contenedor-reproducir-episodio-serie">
									<img class="btn-play-serie" alt="Reproducir Video" longdesc="<?php bloginfo("template_directory"); ?>/img/img_episodios_cenado_btn_play.png" src="<?php bloginfo("template_directory"); ?>/img/img_episodios_cenado_btn_play.png">
									<img class="responsive-img-full-w-h" alt="<?php echo $video->post_title; ?>" longdesc="http://img.youtube.com/vi/<?php the_field("youtube_id", $video->ID); ?>/mqdefault.jpg" src="http://img.youtube.com/vi/<?php the_field("youtube_id", $video->ID); ?>/mqdefault.jpg">
								</div>
								<div class="space20"></div>
								<span class="opns-bold white-text block" style="margin-left: 10px;"><?php echo $programa->post_title; ?></span>
								<span class="opns-regular white-text block" style="margin-left: 10px;"><?php echo $video->post_title; ?></span>
								<br />
							</a>
						</div>
						<?php } ?>
						<div class="space40">
					</div>
				</div>
			<?php } ?>
			<?php if (count($favoritos->posts) > 0) { ?>
				<div class="container-fluid">
					<div class="row no-margin-row">
						<div class="contenedor-titulo-carrusel-interes">
							<span class="opns-bold-italic font22 green-text uppercase">
								Mis Favoritos
							</span>
							<div class="space20"></div>
						</div>
					</div>
					<div class="row carrusel-edit no-margin-row">
						<?php foreach ($favoritos->posts as $item) { $programa = get_post(get_field("programa",$item->ID)); $programs_show[] = $programa->ID; ?>
						<div class="col s6 m6 l3 no-padding" style="margin: 0px 4px;">
							<div class="contenedor-img-carrusel">
								<!--<a href="<?php echo get_permalink($programa->ID); ?>" container=".info-serie-favoritos" loader=".loader-favoritos">-->
								<a href="<?php echo get_permalink($programa->ID); ?>">
									<div class="hover-ver-mas">
										<i class="fa fa-plus-circle white-text" aria-hidden="true"></i>
										<p class="avenir-next-regular font20 white-text">VER MÁS</p>
									</div>
									<img class="responsive-img-full-w-h" alt="<?php echo $item->post_title; ?>" longdesc="<?php the_field("logo",$programa->ID); ?>" src="<?php the_field("logo",$programa->ID); ?>">
								</a>
							</div>
						</div>
						<?php } ?>
					</div>
					<div class="row loader-favoritos centered" style="display: none;">
						<div class="space40"></div>
						<div class="preloader-wrapper big active">
							<div class="spinner-layer spinner-white-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
						<div class="space40"></div>
					</div>
					<div class="row info-serie-favoritos"></div>
				</div>
			<?php } ?>
		<?php } ?>
		
		<!--Carrusel-->
		<?php $featured = get_field('featured', 'option'); ?>
		<div class="container-fluid">
			<div class="row no-margin-row">
					<div class="contenedor-titulo-carrusel-interes">
						<span class="opns-bold-italic font22 green-text uppercase">
							Los más Buscados
						</span>
						<div class="space20"></div>
					</div>
				</div>
			<div class="row carrusel-edit no-margin-row">
				<?php foreach ($featured as $item) { $obj = $item['program'][0]; $programs_show[] = $obj->ID; ?>
				<div class="col s6 m6 l3 no-padding" style="margin: 0px 4px;">
					<div class="contenedor-img-carrusel">
						<!--<a href="<?php echo get_permalink($obj->ID); ?>" container=".info-serie-carrousel" loader=".loader">-->
						<a href="<?php echo get_permalink($obj->ID); ?>">
							<div class="hover-ver-mas">
								<i class="fa fa-plus-circle white-text" aria-hidden="true"></i>
								<p class="avenir-next-regular font20 white-text">VER MÁS</p>
							</div>
							<img class="responsive-img-full-w-h" alt="<?php echo $obj->post_title; ?>" longdesc="<?php the_field("logo",$obj->ID); ?>" src="<?php the_field("logo",$obj->ID); ?>">
						</a>
						<?php if ($item['text']) { ?>
						<div class="info-text-carrusel centered">
							<span class="opns-bold white-text uppercase"><?php echo $item['text']; ?></span>
						</div>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="row loader centered" style="display: none;">
				<div class="space40"></div>
				<div class="preloader-wrapper big active">
					<div class="spinner-layer spinner-white-only">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
				</div>
				<div class="space40"></div>
			</div>
			<div class="row info-serie-carrousel"></div>
		</div>
		
		<?php
			//CONSULTAMOS ALEATORIAMENTE 8 ELEMENTOS
			$args = array(
				'post_type' => 'programa',
				'post__not_in' => $programs_show,
				'order' => 'ASC',
				'orderby' => 'rand',
				'posts_per_page' => 8
			); 
			$randoms = new WP_Query( $args ); 
		
			/* Leemos las Temáticas */
			$tematicas = get_terms( array(
			    'taxonomy' => 'tematica',
			    'hide_empty' => true,
			) );
			shuffle($tematicas);
			$contador_tematicas = 0;
		?>
		
		<?php foreach ($tematicas as $tematica) { $contador_tematicas++; ?>
		<div class="container-fluid">
			<div class="row no-margin-row">
				<div class="contenedor-titulo-carrusel-interes">
					<span class="opns-bold-italic font22 white-text uppercase">
						<?php echo $tematica->name; ?>
					</span>
					<div class="space20"></div>
				</div>
			</div>
			<?php 
				//Consulta de Posts de los Generos
				$args = array(
					'post_type' => 'programa',
					'posts_per_page' => -1,
					'order' => 'ASC',
					'orderby' => 'rand',
					'tax_query' => array(
						array(
							'taxonomy' => 'tematica',
							'field'    => 'slug',
							'terms'    => $tematica->slug ,
						),
					),
				); 
				$programas = new WP_Query( $args ); 
			?>
			<div class="row carrusel-edit no-margin-row">
				<?php foreach ($programas->posts as $item) { ?>
				<div class="col s6 m6 l3 no-padding" style="margin: 0px 4px;">
					<div class="contenedor-img-carrusel">
						<!--<a href="<?php echo get_permalink($item->ID); ?>" container=".info-serie-<?php echo sanitize_title($tematica->name); ?>" loader=".loader-<?php echo sanitize_title($tematica->name); ?>">-->
						<a href="<?php echo get_permalink($item->ID); ?>" >
							<div class="hover-ver-mas">
								<i class="fa fa-plus-circle white-text" aria-hidden="true"></i>
								<p class="avenir-next-regular font20 white-text">VER MÁS</p>
							</div>
							<img class="responsive-img-full-w-h" alt="<?php echo $item->post_title; ?>" longdesc="<?php the_field("logo",$item->ID); ?>" src="<?php the_field("logo",$item->ID); ?>">
						</a>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="row loader-<?php echo sanitize_title($tematica->name); ?> centered" style="display: none;">
				<div class="space40"></div>
				<div class="preloader-wrapper big active">
					<div class="spinner-layer spinner-white-only">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
				</div>
				<div class="space40"></div>
			</div>
			<div class="row info-serie-<?php echo sanitize_title($tematica->name); ?>"></div>
		</div>
		<?php } ?>
		
		<div class="container-fluid">
			<div class="row no-margin-row">
				<div class="space20"></div>
				<div class="row">
					<div class="col s12 m6 offset-m3 l2 offset-l5">
						<div class="space20"></div>
						<a href="#modal_aviso" class="modal-trigger">
							<div class="btnGreen centered">
								<span class="roboto font22 gray-text">AVISO</span>
							</div>
						</a>
					</div>
				</div>
			</div>
			<!-- Modal Structure -->
			<div id="modal_aviso" class="modal">
				<div class="modal-content black">
					<span class="black opns-regular white-text block text-justify"><?php the_field("warning","option"); ?></span>
					<div class="row">
						<div class="col s12 m6 offset-m3 l4 offset-l4">
							<div class="space20"></div>
							<a href="#!" class="modal-action modal-close">
								<div class="btnGreen centered">
									<span class="roboto font22 gray-text">Cerrar</span>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>