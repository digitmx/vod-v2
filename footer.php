<?php $wp_session= WP_Session::get_instance(); ?>

		<!--Footer-->
		<div class="container-fluid">
			<div class="row">
				<div class="col s10 offset-s1 m8 offset-m2 l8 offset-l2 no-padding-med-and-down">
					<div class="space20"></div>
					<div class="col s12 m12 l3 no-padding">
						<div class="contenedor-logo-footer centered">
							<a href="<?php bloginfo("url"); ?>">
								<img class="responsive-img-full-w-h" alt="Video On Demand Canal Once" longdesc="<?php bloginfo("template_directory"); ?>/img/logo_white.png" src="<?php bloginfo("template_directory"); ?>/img/logo_white.png">
							</a>
						</div>
					</div>
					<div class="col s12 m12 l9">
						<div class="space20"></div>
						<p class="opns-semibold font14 grey-text no-padding-med-and-down">
							<?php the_field("copyright", "option"); ?>
						</p>
					</div>
					<!--
					<div class="col l12 hide-on-med-and-down centered">
						<div class="space20"></div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_e5_A.png">
							</a>
						</div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_ipn_A.png">
							</a>
						</div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_ifai_A.png">
							</a>
						</div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_mex_A.png">
							</a>
						</div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_tei_A.png">
							</a>
						</div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_tal_A.png">
							</a>
						</div>
						<div class="space20"></div>
					</div>
					<div class="col s12 m12 hide-on-large-only no-padding-med-and-down centered">
						<div class="space10"></div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_e5_A.png">
							</a>
						</div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_ipn_A.png">
							</a>
						</div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_tei_A.png">
							</a>
						</div>
					</div>
					<div class="col s12 m12 hide-on-large-only no-padding-med-and-down centered">
						<div class="space10"></div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_tal_A.png">
						</div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_ifai_A.png">
							</a>
						</div>
						<div class="contenedor-footer-social">
							<a href="#">
								<img class="responsive-img-full-w-h" src="<?php bloginfo("template_directory"); ?>/img/icon_footer_mex_A.png">
							</a>
						</div>
						<div class="space20"></div>
					</div>
					-->
				</div>
			</div>
		</div>
		
		<input type="hidden" id="base_url" name="base_url" value="<?php bloginfo('url'); ?>" />
		<input type="hidden" id="iduser" name="iduser" value="<?=(isset($wp_session['user']['id'])) ? (string)trim($wp_session['user']['id']) : '0';?>" />
		
		<?php wp_footer(); ?>

	</body>
</html>